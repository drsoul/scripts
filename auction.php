<?php
//set_time_limit(1);//1 sec

set_error_handler('logError');

//обработчик ошибок
function logError($errno, $errstr = '', $errfile = '', $errline = ''){
    $dir = dirname(__FILE__) . '/../logs/auction/'; //"/home/www/dsp/public_html/logs/";//старый путь
    $file = 'auction_' . date('Y_m_d').'.txt';

    if($errno != 8192){
        file_put_contents($dir.$file,$errno."|$errstr|$errline|$errfile\n",FILE_APPEND);
    }
}

require_once( dirname(__FILE__) . '/../db_connect.php' );


###Класс аукциона, весь фукнционал сведён в конструктор
Class Auction extends Db_connect{
    
    private static $instance;
    
    protected $mem_cache;
    protected $mem_cache_net;

    #данные запроса цены
    protected $size;
    protected $domain_check;
    protected $user_id;
    protected $ssp_id_int;
    protected $country_id;
    protected $city_id;
    protected $area_id;
    protected $region_id;
    protected $request_id;
    protected $bidfloor;
    protected $category;
    protected $site_category;
    protected $browser_ids;
    protected $ip_id;
    protected $path_id;
    protected $device_id;
    protected $os_id;
    protected $browser_id;
    protected $path;
    protected $id;
    
    protected $_date;
    protected $_date_other_format;
    protected $user_agent;
    //add 06.06.14
    protected $sex;
    protected $age;
    protected $interest;

    
    //отслеживание времени выполнения фрагментов скрипта
    protected $runtime = 1;
    protected $sql_cache = 'SQL_NO_CACHE'; // 'SQL_NO_CACHE' or ''
    
    
    private static $mobile_detect;
    
    #массив записи времени выполнения
    protected $_a = array();
    #время старта скрипта
    protected $m;
    #отобранный на аукционе баннер
    protected $banner;
    
    public static function I()
    {
        if (!isset(self::$instance)){
            self::$instance = new Auction();
        }
        return self::$instance;
    }
    
    ###в конструкторе собраны все действия скрипта
    public function __construct() {
        
        //установка времени начала выполнения
        $this->m = microtime(true);
        
        //подсключение к базам данных в родительском классе
        parent::__construct();
        
        //время выполнения
        if($this->runtime == 1){
            $tmp_t = microtime(true);
            $this->_a['parent_construct'] = ($tmp_t - $this->m);
            $this->m = $tmp_t; 
        }
        
        //получаем запрос от front bidder`a
        if(!isset($raw_request)){
            //TEST
            //$raw_request = $_GET['req'];//$raw_request = file_get_contents('php://input');
            //$raw_request = file_get_contents('php://input');
			$raw_request = urldecode($_POST['q']);
        }
        $data = json_decode($raw_request);        

	/*
        //TEST
        
            file_put_contents( 'test_log_raw_request.txt' , date("Y-m-d H:i:s") .
                    " raw_request:  " . $raw_request . "\r\n", FILE_APPEND);
        
        //END TEST 
	*/

        //проверяем данные в запросе
        //если данных нет или они в неверном формате завершаем выполнение скрипта
        if(empty($data) or !is_object($data)) die('Not send request!');
        
        //время выполнения
        if($this->runtime == 1){
            $tmp_t = microtime(true);
            $this->_a['get_request'] = ( $tmp_t - $this->m );
            $this->m = $tmp_t; 
        }
        
        ### Memcached соединение
        //TEST
        $this->mem_cache = new Memcached();//$this->mem_cache = new Memcached();
        $this->mem_cache->addServer('/home/www/dsp/memcached.sock', 0); 
        
        //$this->mem_cache_net = new Memcached();
        //$this->mem_cache_net->addServer($this->_memcached_net_host, $this->_memcached_net_port);

        
        //был путь '/home/www/dsp/memcached.sock'
        
        //test memcache
        $this->mem_cache->set('test', 1);
        if( $this->mem_cache->get('test') != 1){
            file_put_contents( 'MEMCACHED_DOWN.txt' , 'MEMCACHED_DOWN!!! Need up him!' . "\r\n");
                        
            //мемкэш упал, останавливаем всё
            die;
        }
        
        //время выполнения
        if($this->runtime == 1){
            $tmp_t = microtime(true);
            $this->_a['mem_cache_init'] = ( $tmp_t - $this->m );
            $this->m = $tmp_t; 
        }
        
        #######################################
        ### записываем данные в свойства класса
        #######################################
        $this->_date = date('Y_m_d');
        $this->_date_other_format = date('Y-m-d');        
        $this->size = $data->size;
        
        //$site_p = parse_url($data->site);
        //$site_tmp = $site_p['host'];
        //$site_tmp = str_replace('www.','', $data->site);

        $site_tmp = preg_replace('/^www\./i', '', $data->site);
        
        $this->domain_check = $site_tmp;
        $this->site = $site_tmp;
        $this->user_id = $data->user_id;
        $this->ssp_id_int = (int)$data->ssp;
        $this->country_id = (int)$data->country;
        $this->city_id = (int)$data->city;
        $this->area_id = (int)$data->area;
        $this->region_id = (int)$data->region;        
        $this->request_id = (int)$data->request_id;
        $this->path = $data->path;
        $this->ip = $data->ip;
        $this->user_agent = $data->user_agent;
        //add 06.06.14
        $this->sex = $data->sex;
        $this->age = $data->age;
        if( !empty($data->interest) ){
            $this->interest = $data->interest;
        }else{
            $this->interest = 0;
        }
        
        //в методе insert_request происходит перезапись свойства $this->request_id
        //обратить внимание (досталось в наследство, возможно баг)
        
        $this->bidfloor = (!empty($data->bidfloor))?(int)$data->bidfloor:0;
        $this->category = $data->category;        
        
        
        //время выполнения
        if($this->runtime == 1){ 
            $tmp_t = microtime(true);
            $this->_a['vars_init'] = ( $tmp_t - $this->m );
            $this->m = $tmp_t;             
        }
        
        /*
        //TEST
        //if(1){
            file_put_contents( 'test_log_site.txt' , date("Y-m-d H:i:s") .
                    " site:  " . $this->site . "\r\n", FILE_APPEND);
        //}
        //END TEST 
        */
              
        /*
        if(!empty($data->browsers_ids)){
            $this->browser_ids = implode(",",$data->browsers_ids);
        }else{
            $this->browser_ids = "0,6";
        }  
        //ВОЗМОЖНО НУЖНО УДАЛИТЬ, мы уже заполнили это переменную
        if( empty($this->browser_ids) ){
            $this->browser_ids = '0';
        }
        */
        
        
        $this->device_detect();
        //TEST
        //$this->device_id = 3;
        //$this->browser_id = 0;
        //$this->os_id = 0;
        //END TEST
        
        //время выполнения
        if($this->runtime == 1){
            $tmp_t = microtime(true);
            $this->_a['device_detect'] = ( $tmp_t - $this->m );
            $this->m = $tmp_t;               
        }        
        
        
        ###установка $this->site_id
        $this->init_site_id();
        
        //время выполнения
        if($this->runtime == 1){             
            $tmp_t = microtime(true);
            $this->_a['init_site_id'] = ( $tmp_t - $this->m );
            $this->m = $tmp_t;              
        } 
        
        //TEST
        //echo '<br>init_site_id complete!';
        //TEST
        //var_dump($this->request_id);
        
        ###установка $this->block
        ###МОЖЕТ ЗАВЕРШИТЬ РАБОТУ СКРИПТА
        $this->init_blocked_sites();
 
        //время выполнения
        if($this->runtime == 1){
            $tmp_t = microtime(true);
            $this->_a['init_blocked_sites'] = ( $tmp_t - $this->m );
            $this->m = $tmp_t;            
        }
        
        //TEST
        //echo '<br>init_blocked_sites complete!';
        
        ###устанавливает $this->site_category
        ###обращается к кэшу, к дочерней бд, к главной бд    
        ###обновляет таблицу site в главной и дочерней базе
        $this->init_site_category(); 

        //время выполнения
        if($this->runtime == 1){
            $tmp_t = microtime(true);
            $this->_a['init_site_category'] = ( $tmp_t - $this->m );
            $this->m = $tmp_t;              
        }
        
        //TEST
        //echo '<br>init_site_category complete!';
        //var_dump($this->_a);
        
        ###выборка подходящих объявлений, нахождение среди них победителя, запись статистики
        ###устанавливает $this->banner (победитель аукциона)
        ###может завершить скрипт
        $this->select_suitable_ads_auction_and_write_stat();

        //время выполнения
        if($this->runtime == 1){
            $tmp_t = microtime(true);
            $this->_a['select_suitable_ads_auction_and_write_stat'] = ( $tmp_t - $this->m );
            $this->m = $tmp_t;
        }
        
        //TEST
        //echo '<br>select_suitable_ads_auction_and_write_stat complete!';
        
        ###если у баннера включен auto_cpm то модифицирует $this->banner['cpm']
        ###в зависимости от количества аукционов для этого пользователя до первого показа
        ###после первого показа инкремент cpm останавливается и этому пользователю 
        ###будут совершаться показы по указанной цене, если она не выше cpm рекламодателя
        //$this->init_auto_cpm();
        //отключил после общения с Николаем (идут постоянные запросы к главной бд, слишком нагружает)
        
        ###модифицирует cpm деля его на ratio (site)
        $this->modif_cpm_by_site_ratio();

        //время выполнения
        if($this->runtime == 1){
            $tmp_t = microtime(true);
            $this->_a['modif_cpm_by_site_ratio'] = ( $tmp_t - $this->m );
            $this->m = $tmp_t;
        }
        
        ###сравнивает cpm с минимальной ставкой для позиции
        ###если cpm ниже то завершает скрипт                
        
        $this->die_by_cpm();
        /*
        //время выполнения
        if($this->runtime == 1){   
            $tmp_t = microtime(true);
            $this->_a['die_by_cpm'] = ( $tmp_t - $this->m );
            $this->m = $tmp_t;
        }
        */
        
        ###устанавливает $this->path_id
        ###работает с кэшем, дочерней бд, главной бд
        $this->init_path();

        //время выполнения
        if($this->runtime == 1){  
            $tmp_t = microtime(true);
            $this->_a['init_path'] = ( $tmp_t - $this->m );
            $this->m = $tmp_t;
        }
        
        ###устанавливает $this->ip_id
        ###работает кэшем, дочерней бд, главной бд
        $this->init_ip_id();

        //время выполнения
        if($this->runtime == 1){
            $tmp_t = microtime(true);
            $this->_a['init_ip_id'] = ( $tmp_t - $this->m );
            $this->m = $tmp_t;
        }
        
        ###устанавливает $this->os_id и $this->browser_id
        ###на основе $data->user_agent
        //$this->init_os_id_and_browser_id();
        
        ###вставляет строку о запросе цены в таблицу requests дочерней бд
        ###перезаписывает $this->request_id 
        $this->insert_request();

        //время выполнения
        if($this->runtime == 1){ 
            $tmp_t = microtime(true);
            $this->_a['insert_request'] = ( $tmp_t - $this->m );
            $this->m = $tmp_t;
        }
        
        ###формирует и выводит ответ в json формате
        $this->echo_response();
        
        //время выполнения
        if($this->runtime == 1){ 
            $tmp_t = microtime(true);
            $this->_a['echo_response'] = ( $tmp_t - $this->m );
            $this->m = $tmp_t;
        }
        
        //время выполнения
        if($this->runtime == 1){
            $sql = 'INSERT INTO runtime_control(name,microtime) VALUES ';
            $sep = '';
            foreach($this->_a as $key => $value){
                $sql .= $sep . '("' . $key . '",' . $value . ')';
                $sep = ',';
            }
            
            $this->q_sub($sql);
            
            //оставляем только 1000 последних строк статистики
            $res = $this->q_sub("SELECT $this->sql_cache (MAX(id) - 1000) as max_del FROM runtime_control");
            $line = $this->fetch_assoc($res);
            $this->q_sub('DELETE FROM runtime_control WHERE id < ' . $line['max_del']);
            $time_n = date('i:s');
            $time_n_arr = explode(':', $time_n);
            
            //сброс таблицы срезов статистики
            if($time_n_arr[1] == 1){//$time_n_arr[0] == 1 AND 
                $this->q_sub('TRUNCATE TABLE runtime_control');
            }
            
            /*
            //TEST
            //if(1){
                file_put_contents( 'test_log_runtime.txt' , date("Y-m-d H:i:s") .
                        
                        ' print_r($this->_a):  ' . print_r($this->_a)  .
                        " data:  " . $this->_a['echo_response'] .
                        " microtime:  " . microtime(true) .
                        " this->_a[count() - 1]:  " . $this->_a[count($this->_a) - 1] .
                        " count():  " . count($this->_a)  .
                        ' $this->_a[1]:  ' . $this->_a[1]  .
                        
                        "\r\n" . 
                        ' $sql:  ' . $sql  .
                        "\r\n" . "\r\n" , FILE_APPEND);
               
            //}
            //END TEST 
            */    
        }
        
        
        //TEST       
        
    }
    
    protected function device_detect(){
        
        /*
        $user_agent_md5 = md5($this->user_agent);
        $u_a_arr = array();
        $u_a_arr = $this->mem_cache->get('u_a_arr_'.$user_agent_md5);
        
        if( !empty($u_a_arr) ){
            $this->device_id = $u_a_arr['device_id'];
            $this->os_id = $u_a_arr['os_id'];
            $this->browser_id = $u_a_arr['browser_id'];
        }else{
        */

            $this->device_id = 0;


            if( $this->m_d()->isTablet($this->user_agent, $httpHeaders = false) ){
                $this->device_id = 2;
            }elseif( $this->m_d()->isMobile($this->user_agent, $httpHeaders = false) ){
                $this->device_id = 1;            
            }else{
                $this->device_id = 3;
            }

            if($this->device_id == 1 OR $this->device_id == 2){
                //определение ОС для мобильных устройств (смартфоны, планшеты и т.д.)
                if( $this->m_d()->isAndroidOS() ){
                    $this->os_id = 6;
                }elseif( $this->m_d()->isiOS() ){
                    $this->os_id = 8;
                }elseif( $this->m_d()->isWindowsPhoneOS() ){
                    $this->os_id = 7;
                }elseif( $this->m_d()->isSymbianOS() ){
                    $this->os_id = 9;
                }elseif( $this->m_d()->isBlackBerryOS() ){
                    $this->os_id = 10;
                }elseif( $this->m_d()->isJavaOS() ){
                    $this->os_id = 11;
                }else{
                    $this->os_id = 0;
                }

                //определение Браузера для мобильных устройств (смартфоны, планшеты и т.д.)
                if( $this->m_d()->isOpera() ){
                    $this->browser_id = 9;
                }elseif( $this->m_d()->isChrome() ){
                    $this->browser_id = 10;
                }elseif( $this->m_d()->isFirefox() ){
                    $this->browser_id = 11;
                }elseif( $this->m_d()->isIE() ){
                    $this->browser_id = 12;
                }elseif( $this->m_d()->isSafari() ){
                    $this->browser_id = 13;
                }elseif( $this->m_d()->isUCBrowser() ){
                    $this->browser_id = 14;
                }elseif( $this->m_d()->isGenericBrowser() ){
                    $this->browser_id = 15;
                }else{
                    $this->browser_id = 0;
                }

            }elseif($this->device_id == 3){
                //определение ОС для стационарных устройств (ПК, ноутбуки и т.д.)
                if( strpos($this->user_agent, 'Windows') !== false ){
                    $this->os_id = 1;
                }elseif( strpos($this->user_agent, 'Mac OS') !== false ){
                    $this->os_id = 2;
                }elseif( strpos($this->user_agent, 'Linux') !== false ){
                    $this->os_id = 3;
                }elseif( strpos($this->user_agent, 'FreeBSD') !== false ){
                    $this->os_id = 4;
                }elseif( strpos($this->user_agent, 'SunOS') !== false ){
                    $this->os_id = 5;
                }else{
                    $this->os_id = 0;
                }

                //определение Браузера для стационарных устройств (ПК, ноутбуки и т.д.)
                if( strpos($this->user_agent, 'Chrome/') !== false ){
                    $this->browser_id = 1;
                }elseif( strpos($this->user_agent, 'Firefox/') !== false ){
                    $this->browser_id = 2;
                }elseif( strpos($this->user_agent, 'Opera/') !== false ){
                    $this->browser_id = 3;
                }elseif( strpos($this->user_agent, 'YaBrowser/') !== false ){
                    $this->browser_id = 4;
                }elseif( strpos($this->user_agent, 'MSIE') !== false ){
                    $this->browser_id = 6;
                }elseif( strpos($this->user_agent, 'Maxthon/') !== false ){
                    $this->browser_id = 7;
                }elseif( strpos($this->user_agent, 'Sleipnir/') !== false ){
                    $this->browser_id = 8;
                }elseif( strpos($this->user_agent, 'Safari') !== false ){
                    $this->browser_id = 5;
                }else{
                    $this->browser_id = 0;
                }            

            
            } 
            
            /*
            $u_a_arr['device_id'] = $this->device_id;
            $u_a_arr['os_id'] = $this->os_id;
            $u_a_arr['browser_id'] = $this->browser_id;
            
            $this->mem_cache->set('u_a_arr_'.$user_agent_md5, $u_a_arr);
            
        
        }
        */
        
        //TEST
        /*
        $line = ''; 
        foreach($this->m_d()->getProperties() as $name => $match){
            $check = $this->m_d()->version($name);
            if($check!==false){  
                $line .= " version($name) = $check ";
            }
        }
        */
        /*
        file_put_contents( 'test_log_device_type.txt' , date("Y-m-d H:i:s") .
        " device_id: $this->device_id " . " os_id: $this->os_id " .
        " browser_id_new: $this->browser_id_new " .
        " ----- user_agent: $this->user_agent " . "\r\n", FILE_APPEND);
        */
        //END TEST
        
    }
    
    public static function m_d(){
        
        include_once( dirname(__FILE__) . '/../libs/Mobile_Detect.php' );
        
        if (!isset(self::$mobile_detect)){
            self::$mobile_detect = new Mobile_Detect();
        }
        return self::$mobile_detect;
    }
    
    
    ###устанавливает $this->site_id
    ###обращается к кэшу и к дочерней и главной бд
    protected function init_site_id(){
        

        
        ### Установка $this->site_id
        $this->site_id = $this->mem_cache->get('site_id_'.$this->site);
        /*
        //TEST                       
        file_put_contents( 'test_log_site_id.txt' , date("Y-m-d H:i:s") .
                " site_id:  " . $this->site_id . "\r\n", FILE_APPEND);        
        //END TEST
        */
        
        if(empty($this->site_id)){
            //если кэш пуст делаем выборку из дочерней базы
            $sql = "SELECT $this->sql_cache site_id, category_rtb FROM site WHERE domain='".$this->site."'";
            $res = $this->fetch_assoc_sub($sql);
            
            $this->site_id = (int)$res['site_id'];
            $this->site_category = (int)$res['category_rtb'];
            /*
            //TEST
            echo '<br>$this->site_id ';
            var_dump($this->site_id);
            echo '<br>$this->site ';
            var_dump($this->site);
            */
            
           
            /*
            //проверяем не был ли добавлен это сайта в бд с другим вариантом www.
            if($this->site_id == 0 ){
                if( strpos($this->site, 'www.') === false){
                    $site_domain_new = 'www.' . $this->site;
                }else{
                    $site_domain_new = str_replace('www.', '', $this->site);
                }
                
                $sql = "SELECT $this->sql_cache site_id, category_rtb FROM site WHERE domain='".$site_domain_new."'";
                $res = $this->fetch_assoc_sub($sql);
                $this->site_id = (int)$res['site_id'];
                $this->site_category = (int)$res['category_rtb'];
            }
            */
            //TEST
            //echo '<br>$this->site_id 2 ';
            //var_dump($this->site_id);           
            
            //если дочерняя бд пуста делаем выборку из главной бд
            if($this->site_id == 0 ){
                
                //TEST
                //echo '<br>$this->site_id 2 ';
                //var_dump($this->site_id); 
                $resourse = $this->q_main($sql);
                $res = $this->fetch_assoc($resourse);
                //res = $this->fetch_assoc_main($sql);
                /*
                //TEST                       
                file_put_contents( 'test_log_site_id.txt' , date("Y-m-d H:i:s") .
                        " res[site_id]:  " . $res['site_id'] . "\r\n", FILE_APPEND);        
                //END TEST
                */
                $this->site_id = (int)$res['site_id'];
                $this->site_category = (int)$res['category_rtb'];
            }
            
        }

        

        //если главная бд пуста делаем вставку через api megaindex
        if($this->site_id == 0){
            //TEST
            //$this->site_id = 9983785;//$this->site_id = (int)file_get_contents('http://www.megaindex.ru/site_api.php?site=' . $this->site);
            $this->site_id = (int)file_get_contents('http://www.megaindex.ru/site_api.php?site=' . $this->site);
            
            //полученный site_id вставляем в главную и дочернюю базу (синхронизация)
            $sql = "INSERT INTO site (site_id, domain) VALUES ('$this->site_id','".$this->site."')";            
            $this->q_main($sql);
            //$this->q_sub($sql);
            $this->site_id = $this->insert_id_main(); 
        }
        
        $this->mem_cache->set('site_id_'.$this->site, $this->site_id);
        ### КОНЕЦ - Установка $this->site_id
        
        /*
        //TEST       
        if(strpos($this->site, 'www.zaycev.net') !== false){
            file_put_contents( 'test_log_www.zaycev.net.txt' , date("Y-m-d H:i:s") .
                    " site_id:  " . $this->site_id .
                    " site:  " . $this->site . "\r\n", FILE_APPEND); 
        }elseif(strpos($this->site, 'zaycev.net') !== false){
            file_put_contents( 'test_log_zaycev.net.txt' , date("Y-m-d H:i:s") .
                    " site_id:  " . $this->site_id .
                    " site:  " . $this->site . "\r\n", FILE_APPEND);  
        }
        //END TEST        
        */
        
    }
    
    ###устанавливает $this->block
    ###обращается к кэшу и к дочерней бд
    ###МОЖЕТ ЗАВЕРШИТЬ РАБОТУ СКРИПТА
    protected function init_blocked_sites(){        
        
        $this->block = $this->mem_cache->get('blocked_sites_'.$this->site_id);

        if(empty($this->block)){
            $this->block = $this->fetch_assoc_sub("SELECT $this->sql_cache id FROM blocked_sites WHERE site_id = '$this->site_id'");
            $this->block = (int)$this->block['id'];
            if($this->block > 0){
                $this->mem_cache->set('blocked_sites_'.$this->site_id, $this->block);
            }else{
                $this->mem_cache->set('blocked_sites_'.$this->site_id, -1, 300);
            }
        }
        
        //если запросивший рекламу сайт заблокирован то будет завершён скрипт
        if($this->block > 0){
            die();
        }        
    }
    
    ###устанавливает $this->site_category
    ###обращается к кэшу, к дочерней бд, к главной бд    
    ###обновляет таблицу site в главной и дочерней базе
    protected function init_site_category(){ 
        
        /*
        //TEST
        //if($user_id == '1417442273' OR $this->_data->device->ip == '95.37.109.153'){
            file_put_contents( 'auc_category.txt' , date("Y-m-d H:i:s") .
                    " this->category: $this->category " . "\r\n", FILE_APPEND);
        //}
        //END TEST
        */
        
        //если site_category ещё не инициирована то делаем это
        if( empty($this->site_category) AND $this->site_category !== 0){        
            if(!empty($this->category)){
                /*
                //TEST
                //if(1){
                    file_put_contents( 'test_log_cat.txt' , date("Y-m-d H:i:s") .
                            " this->category: $this->category " . "\r\n", FILE_APPEND);
                //}
                //END TEST              
                */

                //$category = explode("-",$this->category);
                //$category = $category[0];         


                $this->site_category = $this->mem_cache->get('category_site_new_' . $this->category);
                
                
                if(empty($this->site_category)){
                    $cat_id = $this->fetch_assoc_sub("SELECT $this->sql_cache id FROM `rtb_categories` WHERE `code` = '$this->category'");
                    $this->site_category = (int)$cat_id['id'];
                    $this->mem_cache->set('category_site_new_'.$this->category, $this->site_category);
                    //было в старой версии $cat_id вместо $this->site_category

                    //обновляем в главной и дочерней бд

                    //$this->q_sub($sql);
                }



            }else{
                $this->site_category = 29;
            }
            
            $sql = "UPDATE site SET category_rtb = '$this->site_category' WHERE site_id = '$this->site_id'";
            $this->q_main($sql);
        }
        

    }
    
    
    
    ###выборка подходящих объявлений, нахождение среди них победителя, запись статистики
    ###устанавливает $this->banner (победитель аукциона)
    ###может завершить скрипт
    ###работает с таблицами дочерней бд (стрелкой обозначено направление синхронизации баз данных):
    #target SELECT 678 доч. <- глав.
    #ads SELECT 419 доч. <- глав.
    #site SELECT 910к доч. <- глав.    
    #company SELECT 376 доч. <- глав.
    #products_u2t SELECT 67к доч. <- глав.
    #ctarget SELECT 921к доч. <- глав.
    #target_geo SELECT 69к доч. <- глав.
    #retarget SELECT 429 доч. <- глав.
    #time_target SELECT 26к доч. <- глав.
    #
    #blocked_sites_ads SELECT 26 доч. <- глав.
    #
    #day_stat INSERT 2к доч. -> глав.
    #target_stats INSERT 2к доч. -> глав.
    #day_stats_hour INSERT 23к доч. -> глав.
    protected function select_suitable_ads_auction_and_write_stat(){ 
        
        //TEST
        //echo $this->domain_check;
        
        /*
        $b_sql = '';
        if($bidfloor > 0){
            $b_sql = " and t.cpm >= '$bidfloor'";
        }else{
            $b_sql = '';
        }
        */
        //закоментировал потому что $b_sql нигде не используется
        
        /*
        $days = array('Mon' => 0, 'Tue'=>1, 'Wed'=>2, 'Thu' => 3, 'Fri'=>4, 'Sat'=>5, 'Sun'=>6);
        $times = array("00:00" => 0,"01:00" => 1,"02:00" => 2,"03:00" => 3,"04:00" => 4,"05:00" => 5,"06:00" => 6,"07:00" => 7,"08:00" => 8,"09:00" => 9,"10:00" => 10,"11:00" => 11,"12:00" => 12,"13:00" => 13,"14:00" => 14,"15:00" => 15,"16:00" => 16,"17:00" => 17,"18:00" => 18,"19:00" => 19,"20:00" => 20,"21:00" => 21,"22:00" => 22,"23:00" => 23);
        $_day_id = $days[date('D')];
        $_time_id = $times[date('H:00')];
        */
        
        //$this->_a[] = 'pre query: '.(microtime(true)-$this->m);

        /*
        //TEST
        //if(1){
            $sql = "SELECT COUNT(*) as count FROM target as t
            INNER JOIN ads as ads ON ads.id=t.ads_id and ads.size='$this->size' and ads.runing=1 and ads.moderation != 2 and ads.deleted = 0
            INNER JOIN site as s ON s.site_id=t.site_id
            INNER JOIN company as rc ON rc.id=ads.company_id and rc.runing = 1 and rc.balance > 1
            WHERE
            t.cpm != 0 and t.blocked = 0
            and (t.product = 0 or (SELECT cc.id FROM products_u2t as cc WHERE cc.user_id='$this->user_id' and cc.target_id = t.id LIMIT 1) > 0)
            and (t.ssp_id = 0 or t.ssp_id = '$this->ssp_id_int')
            and (t.site = '' or t.site='$this->domain_check')
            and (t.browser_id = 0 or t.browser_id in ($this->browser_ids))
            and (t.ctarget = 0 or (SELECT COUNT(*) FROM ctarget as cc WHERE cc.cookie_id='$this->user_id' and cc.cat_id = t.ctarget LIMIT 1) > 0)
            and (t.category_site = 0 OR t.category_site = $this->site_category OR ((SELECT COUNT(*) FROM rtb_categories as rtb WHERE rtb.parent_id = t.category_site AND rtb.id = $this->site_category LIMIT 1) > 0) )
            and (t.geo = 0 or (
                    SELECT tg.id FROM `target_geo` as tg WHERE tg.target_id = t.id
                    and (tg.country_id = $this->country_id)
                    and (tg.area_id = 0 OR tg.area_id = $this->area_id)
                    and (tg.region_id = 0 OR tg.region_id = $this->region_id)
                    and (tg.city_id = 0 OR tg.city_id= $this->city_id)
                    LIMIT 1) > 0)
            and (t.retarget = 0 or ((SELECT COUNT(*) FROM retarget as cc WHERE cc.cookie_id = '$this->user_id' and cc.ads_id=ads.id LIMIT 1) > 0))
            and (t.time_active = 0 or (SELECT tt.id FROM time_target as tt WHERE tt.target_id = t.id and tt.day_id='$_day_id' and tt.time_id='$_time_id' LIMIT 1) > 0)
            and t.active=1
            ORDER BY t.cpm DESC";
        
            $auc_count = $this->fetch_assoc_sub($sql);
            
            if( $auc_count['count'] > 10 ){
                file_put_contents( 'test_log_auc_count.txt' , date("Y-m-d H:i:s") .
                        " targets_for_one_auction: " . $auc_count['count'] . "\r\n", FILE_APPEND);
            }
        //}
        //END TEST  
        */
        
        /*
        $this->sex;
        $this->age;
        $this->interest;
        */
        
        $sql = "SELECT $this->sql_cache * FROM targets_for_auction as t
        WHERE t.size='$this->size' AND t.cpm > '$this->bidfloor' AND t.cpm > 0
        AND t.max_cpm > '$this->bidfloor'
        AND t.balance >= 1
            
        AND (t.age = 0 OR t.age = $this->age) 
        AND (t.sex = 0 OR t.sex = $this->sex)  
        AND (t.interest = 0 OR t.interest IN($this->interest))    

        AND (t.product = 0 or (SELECT cc.id FROM products_u2t as cc WHERE cc.user_id='$this->user_id' and cc.target_id = t.target_id LIMIT 1) > 0)
        AND (t.ssp_id = 0 or t.ssp_id = '$this->ssp_id_int')
        AND (t.site = '' or FIND_IN_SET('$this->domain_check', t.site) > 0)        
        
        AND (t.category_site = 0 OR t.category_site = $this->site_category OR ((SELECT COUNT(*) FROM rtb_categories as rtb WHERE rtb.parent_id = t.category_site AND rtb.id = $this->site_category LIMIT 1) > 0) )
        AND (t.geo = 0 or (
                SELECT tg.id FROM `target_geo` as tg WHERE tg.target_id = t.target_id
                and (tg.country_id = $this->country_id)
                and (tg.area_id = 0 OR tg.area_id = $this->area_id)
                and (tg.region_id = 0 OR tg.region_id = $this->region_id)
                and (tg.city_id = 0 OR tg.city_id= $this->city_id)
                LIMIT 1) > 0)
        AND (t.retarget = 0 or ((SELECT COUNT(*) FROM retarget as cc WHERE cc.cookie_id = '$this->user_id' and cc.ads_id=t.id LIMIT 1) > 0))                   
        AND (t.device_id=0 OR t.device_id = $this->device_id)
        AND (t.os_id=0 OR t.os_id = $this->os_id)
        AND (t.browser_id_new=0 OR t.browser_id_new = $this->browser_id)
        AND (t.views_per_user=0 OR (SELECT a_u_v.views FROM ads_user_views as a_u_v WHERE a_u_v.user_id='$this->user_id' AND a_u_v.ads_id=t.id LIMIT 1) <= t.views_per_user
            OR (SELECT COUNT(a_u_v.id) FROM ads_user_views as a_u_v WHERE a_u_v.user_id='$this->user_id' AND a_u_v.ads_id=t.id LIMIT 1) = 0)
        
        AND (t.max_sum_hour_t = 0 OR (t.expense_hour + 1 < t.max_sum_hour_t))
        AND (t.max_sum_day_t = 0 OR (t.expense_day + 1 < t.max_sum_day_t))
        ORDER BY t.cpm DESC";
        
        //and (t.ctarget = 0 or (SELECT COUNT(*) FROM ctarget as cc WHERE cc.cookie_id='$this->user_id' and cc.cat_id = t.ctarget LIMIT 1) > 0)
        
        ////and (t.browser_id = 0 or t.browser_id in ($this->browser_ids)) //было до 03_04_14
        //t.site='$this->domain_check'
        
        $count_all = 0;
        $r = $this->q_sub($sql);
        //$this->_a[] = 'query: '.(microtime(true) - $this->m);
        
        /*
        //TEST
        echo '<br>$r <pre>';
        print_r($r);
        echo '</pre>';        
        */
        
        
        /* 
        //TEST
        if( empty($r) ){
            file_put_contents( 'test_log_r_auc.txt' , date("Y-m-d H:i:s") .
                     "\r\n" . 'EMPTY RESPONSE $sql: ' . $sql . "\r\n\r\n\r\n", FILE_APPEND);
        }else{
            //file_put_contents( 'test_log_r_auc.txt' , date("Y-m-d H:i:s") .
            //       "\r\n" . 'GOOD RESPONSE $sql: ' . $sql . "\r\n\r\n\r\n", FILE_APPEND);            
        }
        //END TEST
        */
        
        /*
        //если ничего подходящего не удалось найти в бд то завершаем скрипт
        if($r === FALSE){
            
            //TEST
            file_put_contents( 'test_log_bad_auc1.txt' , date("Y-m-d H:i:s") .
                     "\r\n", FILE_APPEND);        
            //END TEST
            
             die();
        }
        */
        
        /*
        if($this->num_rows($r) < 1){
            //подходящих целей нет
            die;
        }   
        */ 
        
        while($rr = $this->fetch_array($r)){
            
            
            if($rr['views_per_user'] != 0){
                //если этот баннер уже показывался этому человеку в определённый период то
                //не показываем пока период не истечёт
                //в этот период происходит синхронизация таблицы с метками о количестве
                //показов кадого баннера кадому человеку
                if( $this->mem_cache->get('u_a_v_'.$this->user_id.'_'.$rr['id']) == 1 ){
                    
                    continue;
                }
            }
            

            if($this->mem_cache->get('block_by_limit_sum_or_views-'.$rr['target_id']) == 1){
                //была блокировка (n_url) по лимиту показов или сумме, действует 70 сек,
                //в течении этого времени пересоберётся дамп аукциона без этого таргета (учавствует крон)
                continue;
            }
            
            //предотвращение торгов для баннеров с закончившимся балансом
            $balance_mem = $this->mem_cache->get('balance_multi_1000_'.$rr['company_id']);
            if( !empty($balance_mem) OR $balance_mem === 0 ){
                $this->mem_cache->decrement( 'balance_multi_1000_'.$rr['company_id'], $rr['cpm'] );            
                            
                if($balance_mem < 1000){ //1000 = 1руб, т.к. баланс домножен на 1к в n_url,
                //там он занесён в мем, это нужно для работы декремента
                    continue;
                }
            }
            /*
            //TEST       
            $rr_json = json_encode($rr);
            file_put_contents( 'test_log_auc.txt' , date("Y-m-d H:i:s") .
                    " rr_json:  " . $rr_json . "\r\n", FILE_APPEND);        
            //END TEST
            */
            
            /*
            //TEST
            echo '<br><pre>';
            print_r($rr);
            echo '</pre>';
            */
            
            
            //$check_block = $this->fetch_assoc_sub("SELECT COUNT(*) as c FROM blocked_sites_company WHERE company_id = '".$rr['company_id']."' and site_id = '$this->site_id'");
            

            
            
            if(!$this->blocked_sites_company($rr['company_id'])){
            
                
                if(!$this->blocked_sites_ads($rr['id'])){


                    /*
                    //old 26_05_14
                    $this->q_sub("INSERT INTO day_stat (banner_id, date, bids) VALUES ('".$rr['id']."', '".$this->_date_other_format."', 1) ON DUPLICATE KEY UPDATE bids=bids+1");
                    $this->q_sub("INSERT INTO target_stats (target_id, date, requests) VALUES ('".$rr['target_id']."', '".$this->_date_other_format."', 1) ON DUPLICATE KEY UPDATE requests=requests+1");
                    $this->q_sub("INSERT INTO day_stats_hour (banner_id, date, bids,hour) VALUES ('".$rr['id']."', '".$this->_date_other_format."', 1, '".date('H')."') ON DUPLICATE KEY UPDATE bids=bids+1");
                    */

                    /*
                    $this->mem_cache->delete('day_stat');
                    $this->mem_cache->delete('target_stats');
                    $this->mem_cache->delete('day_stats_hour');                
                    */


                    ###запись статистики в мемкэш для дальнейшего слива в бд по крону
                    $day_stat_arr = $this->mem_cache->get('day_stat');
                    if( !empty($day_stat_arr[$rr['id'] . '|' . $this->_date_other_format]) ){
                        $day_stat_arr[$rr['id'] . '|' . $this->_date_other_format]++;
                    }else{
                        $day_stat_arr[$rr['id'] . '|' . $this->_date_other_format] = 1;
                    }
                    $this->mem_cache->set('day_stat', $day_stat_arr, 604800); //604800 = 7day                                


                    $target_stats_arr = $this->mem_cache->get('target_stats');
                    if( !empty($target_stats_arr[$rr['target_id'] . '|' . $this->_date_other_format]) ){
                        $target_stats_arr[$rr['target_id'] . '|' . $this->_date_other_format]++;
                    }else{
                        $target_stats_arr[$rr['target_id'] . '|' . $this->_date_other_format] = 1;
                    }
                    $this->mem_cache->set('target_stats', $target_stats_arr, 604800);


                    $day_stats_hour_arr = $this->mem_cache->get('day_stats_hour');
                    $h_tmp = date('H');
                    if( !empty($day_stats_hour_arr[$rr['id'] . '|' . $this->_date_other_format . '|' . $h_tmp]) ){
                        $day_stats_hour_arr[$rr['id'] . '|' . $this->_date_other_format . '|' . $h_tmp]++;
                    }else{
                        $day_stats_hour_arr[$rr['id'] . '|' . $this->_date_other_format . '|' . $h_tmp] = 1;
                    }
                    $this->mem_cache->set('day_stats_hour', $day_stats_hour_arr, 604800); 

                    //ставки по ссп
                    $ssp_day_stat_arr = $this->mem_cache->get('ssp_day_stat');                
                    if( !empty($ssp_day_stat_arr[$this->ssp_id_int . '|' . $this->_date_other_format]) ){
                        $ssp_day_stat_arr[$this->ssp_id_int . '|' . $this->_date_other_format]++;
                    }else{
                        $ssp_day_stat_arr[$this->ssp_id_int . '|' . $this->_date_other_format] = 1;
                    }
                    $this->mem_cache->set('ssp_day_stat', $ssp_day_stat_arr, 604800); 




                    /*
                    //TEST
                    //$this->mem_cache->set('day_stat', 123, 604800);
                    $day_stat_arr = $this->mem_cache->get('day_stat');
                    file_put_contents( 'test_log_day_stat_arr.txt' , date("Y-m-d H:i:s")
                              . '$day_stat_arr: ' . $day_stat_arr[$rr['id'] . '|' . $this->_date_other_format] . "\r\n", FILE_APPEND);        
                    //END TEST
                    //TEST
                    $target_stats_arr = $this->mem_cache->get('target_stats');
                    file_put_contents( 'test_log_target_stats_arr.txt' , date("Y-m-d H:i:s")
                              . '$target_stats_arr: ' . $target_stats_arr[$rr['target_id'] . '|' . $this->_date_other_format] . "\r\n", FILE_APPEND);        
                    //END TEST
                    //TEST
                    $day_stats_hour_arr = $this->mem_cache->get('day_stats_hour');
                    file_put_contents( 'test_log_day_stats_hour_arr.txt' , date("Y-m-d H:i:s")
                              . '$day_stats_hour_arr: ' . $day_stats_hour_arr[$rr['id'] . '|' . $this->_date_other_format . '|' . $h_tmp] . "\r\n", FILE_APPEND);        
                    //END TEST
                    */

                    /*
                    //TEST
                    echo '<br>day_stat';
                    var_dump($rr['id']);
                    echo '<br>target_id';
                    var_dump($rr['target_id']);
                    */
                    
                    //падение cpm при подходе баланса к низкому уровню
                    //для предотвращения проскальзывания
                    //if($rr['geo'] == 1){
                    //$balance_dem = round($rr['balance'] / 2);
                    
                    
                    
                    
                    $balance_dem = round($rr['balance']*8);                    
                    if($balance_dem < 1){
                        $balance_dem = 1;
                    }
                    if($balance_dem < $rr['cpm']){
                        $rr['cpm'] = $balance_dem;
                    }
                    //12.08.14
                    if($rr['cpm'] > $rr['max_cpm']){
                        $rr['cpm'] = $rr['max_cpm'];
                    }
                    
                    
                    
                    if($rr['cpm'] > $this->bidfloor){
                        $count_all++;
                        $rr['price'] = $rr['cpm'];
                        $rrr[] = $rr;
                        $s[] = $rr['cpm'];
                        $g[$rr['id']] = $rr['cpm'];
                    }
                }/*else{

                    //TEST
                    file_put_contents( 'test_log_blacklist.txt' , date("Y-m-d H:i:s") .
                             "\r\n", FILE_APPEND);        
                    //END TEST
                }             
                */
            }
            
        }

        //если не нашли подходяших баннеров завершаем скрипт
        if(empty($rrr)){
            /*
            //TEST
            file_put_contents( 'test_log_empty_banner.txt' , date("Y-m-d H:i:s") .
                     "\r\n", FILE_APPEND);        
            //END TEST
            */    
            die();
        }
        
        //TEST
        $ban_id = $this->auction_get_ban_id_new_ver($s);//$ban_id = $this->auction_get_ban_id($s);


        //$this->_a[] = 'get banner id: '.(microtime(true)-$this->m);

        $banner = $rrr[$ban_id];
        
        //если в массиве баннеров нет банера с таким id то завершаем скрипт
        if(empty($banner)) die();
        
        
        if($banner['views_per_user'] != 0){
            //запоминание показа баннера данному человеку
            $this->mem_cache->set('u_a_v_'.$this->user_id.'_'.$banner['id'], 1, 20);
        }
        
        $this->banner = $banner;
    }
    
    
    protected function blocked_sites_company($company_id){
        $bsc = $this->mem_cache->get('blocked_sites_company_|'.$company_id.'|'.$this->site_id);
        $bsc_common = $this->mem_cache->get('blocked_sites_company_|'.$company_id.'|common');
        //проверяем есть ли сайт в блокировочном кеше компании и была ли выборка блокировок из базы в кэш
        if( empty($bsc) AND !empty($bsc_common) ){
            //блокировки сайта нет, выборка из базы была. Сайт не заблокирован.
            return false;

        }elseif( !empty($bsc) AND !empty($bsc_common) ){
            //сайт заблочен
            return true;            
        }elseif( empty($bsc_common) ){//небыло выборки из бд в кэш 

            //делаем выборку блокированных сайтов в кэш
            $result_bsc = $this->q_sub("SELECT $this->sql_cache site_id FROM blocked_sites_company WHERE company_id = '".$company_id."'");

            while($res_bsc = $this->fetch_assoc($result_bsc)){
                $this->mem_cache->set('blocked_sites_company_|'.$company_id.'|'.$res_bsc['site_id'], 1, 300);
            }
            $this->mem_cache->set('blocked_sites_company_|'.$company_id.'|common', 1, 300);

            //проверяем в кеше сайт
            if( empty($this->mem_cache->get('blocked_sites_company_|'.$company_id.'|'.$this->site_id)) ){
                return false;
            }else{
                return true;
            }                   

        }
    }
    
    
    protected function blocked_sites_ads($ads_id){
        $bsc = $this->mem_cache->get('blocked_sites_ads_id_|'.$ads_id.'|'.$this->site_id);
        $bsc_common = $this->mem_cache->get('blocked_sites_ads_id_|'.$ads_id.'|common');
        //проверяем есть ли сайт в блокировочном кеше компании и была ли выборка блокировок из базы в кэш
        if( empty($bsc) AND !empty($bsc_common) ){
            //блокировки сайта нет, выборка из базы была. Сайт не заблокирован.
            return false;

        }elseif( !empty($bsc) AND !empty($bsc_common) ){
            //сайт заблочен
            return true;            
        }elseif( empty($bsc_common) ){//небыло выборки из бд в кэш 

            //делаем выборку блокированных сайтов в кэш
            $result_bsc = $this->q_sub("SELECT $this->sql_cache site_id FROM blocked_sites_ads WHERE ads_id = '".$ads_id."'");

            while($res_bsc = $this->fetch_assoc($result_bsc)){
                $this->mem_cache->set('blocked_sites_ads_id_|'.$ads_id.'|'.$res_bsc['site_id'], 1, 300);
            }
            $this->mem_cache->set('blocked_sites_ads_id_|'.$ads_id.'|common', 1, 300);

            //проверяем в кеше сайт
            if( empty($this->mem_cache->get('blocked_sites_ads_id_|'.$ads_id.'|'.$this->site_id)) ){
                return false;
            }else{
                return true;
            }                   

        }
    }   
    
    
    protected function auction_get_ban_id_new_ver($s){
        
        $sum = 0;
        foreach($s as $k => $v){
            if($v > 0){
                $sum += $v;
            }
        }
        
        $rand = rand(0, $sum);
        
        /*
        //TEST
        echo '<br>$sum ' . $sum;
        echo '<br>$rand ' . $rand;         
        */
        $sum_t = 0;
        foreach($s as $k => $v){
            if($v > 0){
                $sum_t += $v;
                if($rand <= $sum_t){
                    $ban_id = $k;
                    break;
                }
            }
        }
        
        return $ban_id;
    }

    ###находит победителя среди предварительно отобранных баннеров
    ### params
    ### array $s - массив элементы которого содержат cpm отобранных баннеров,
    ### количество элементов у этого массива должно равняться количеству элементов 
    ### массива из которого будет выбран баннер победитель.
    ### return int $ban_id - id баннера победителя
    protected function auction_get_ban_id($s){
        
        /*
        //TEST
        echo '$s ';
        var_dump($s);
        */
        
        $m = max($s);

        $d = array();

        foreach($s as $k=>$ss){
            $d[$k] = round((($m - $ss)/$m)*100);
        }
        
        /*
        //TEST
        echo '$d ';
        var_dump($d);
        */
        
        if(count($s) == 0){
            die();
        }
        
        //TEST
        //$a = 5;
        $i = array();
        $int = 0;

        foreach($d as $k=>$dd){
            //TEST
            $i[$k] = 100 - $dd;//$i[$k] = pow((100 - $dd),$a);
            $int += $i[$k];
        }
        
        /*
        //TEST
        echo '$i[$k] ';
        var_dump($i);
        var_dump($int);
        */

        $r = rand(0, $int);
        
        /*
        //TEST
        //echo rand(0, 2439404);
        echo '$r ';
        var_dump($r);
        */
        
        $cur = 0;
        $ban_id = 0;
        //TEST
        for($j=0;$j<=count($s);$j++){//$j=1
            $cur += (isset($i[$j]))?$i[$j]:0;
            
            //TEST
            //echo '<br>$cur ' . $cur;
            
            if($r<=$cur){
                /*
                //TEST
                echo '<br>loop ';
                var_dump($cur);
                */
                
                $ban_id= $j;
                break;
            }
        }
        
        //TEST
        //echo '<br>$ban_id ' . $ban_id;
        
        return $ban_id;
    }
    
    
    ###если у баннера включен auto_cpm то модифицирует $this->banner['cpm']
    ###в зависимости от количества аукционов для этого пользователя до первого показа
    ###после первого показа инкремент cpm останавливается и этому пользователю 
    ###будут совершаться показы по указанной цене, если она не выше cpm рекламодателя
    protected function init_auto_cpm(){

        if($this->banner['auto_cpm'] == 1){
            if($this->user_id > 0){
                $ur = $this->fetch_assoc_main("SELECT cpm, view FROM user_cpm WHERE user_id = '$this->user_id'");
                
                //проверяем есть ли запись в таблице
                if($ur['cpm'] > 0){
                    //если показов небыло делаем инкримент cpm
                    if($ur['view'] == 0){
                        $cpm = $ur['cpm']+0.1;
                        $this->q_main("UPDATE user_cpm SET cpm = '$cpm' WHERE user_id = '$this->user_id'");
                    }else{
                        //если показы было то записываем текущий cpm
                        $cpm = $ur['cpm'];
                    }
                    //проверяем не выше ли полученный cpm заданного рекламодателем
                    if($cpm < $this->banner['cpm']){                        
                        $this->banner['cpm'] = $cpm;
                    }

                //если нет то делаем и устанавливаем стартовый cpm
                }else{
                    $this->banner['cpm'] = 0.1;
                    $this->q_main("INSERT INTO user_cpm (user_id, cpm, view) VALUES ('$this->user_id', '0.1', 0)");
                }
            }else{
                $this->banner['cpm'] = 0.1;
            }
        }
    }
    
    ###модифицирует cpm деля его на ratio (site)
    protected function modif_cpm_by_site_ratio(){        
        
        $site_ratio = $this->mem_cache->get('site_ratio_'.$this->site_id);
        if(empty($site_ratio)){
            $site_ratio = $this->fetch_assoc_sub("SELECT $this->sql_cache ratio FROM site WHERE site_id = '$this->site_id'");
            $site_ratio = (int)$site_ratio['ratio'];
            
            $this->mem_cache->set('site_ratio_'.$this->site_id, $site_ratio);
        }

        if($site_ratio > 1){
            $this->banner['cpm'] = $this->banner['cpm'] / $site_ratio;
        }
    }
    
    ###сравнивает cpm с минимальной ставкой для позиции
    ###если cpm ниже то завершает скрипт
    protected function die_by_cpm(){
        if($this->banner['cpm'] < $this->bidfloor){
            die();
        }
    }
    
    ###устанавливает $this->path_id
    ###работает с кэшем, дочерней бд, главной бд
    protected function init_path(){
        $path = $this->path;
        $this->path_id = $this->mem_cache->get('path_id_'.$path);
        
        //если в кэше нет то делаем выборку из дочерней бд
        if(empty($this->path_id)){
            $this->path_id = $this->fetch_assoc_sub("SELECT $this->sql_cache id FROM paths WHERE path='$path'");
            $this->path_id = (empty($this->path_id['id']))?0:(int)$this->path_id['id'];
            
            //если в дочерней нет то делаем выборку из главной бд
            if($this->path_id == 0){
                $resourse = $this->q_main("SELECT $this->sql_cache id FROM paths WHERE path='$path'");
                $this->path_id = $this->fetch_assoc($resourse);
                //$this->path_id = $this->fetch_assoc_main("SELECT id FROM paths WHERE path='$path'");
                /*
                //TEST                       
                file_put_contents( 'test_log_path_id.txt' , date("Y-m-d H:i:s") .
                        " this->path_id:  " . $this->path_id . "\r\n", FILE_APPEND);        
                //END TEST
                */
                $this->path_id = (empty($this->path_id['id']))?0:(int)$this->path_id['id'];
                
                //если в главной нет то делаем вставку в главную бд
                if($this->path_id == 0){
                    $sql = "INSERT INTO paths (path) VALUES ('$path')";
                    $this->q_main($sql);
                    //$this->q_sub($sql);
                    $this->path_id = $this->insert_id_main();
                }
            }
            $this->mem_cache->set('path_id_'.$path, $this->path_id);
        }


        //$this->_a[] = 'get path id: '.(microtime(true) - $this->m);
    }
    
    ###устанавливает $this->ip_id
    ###работает кэшем, дочерней бд, главной бд
    protected function init_ip_id(){
        
        $ip = $this->ip;
        $this->ip_id = $this->mem_cache->get('ip_id_'.$ip);
        
        //если в кэше нет то делаем выборку из дочерней бд
        if(empty($this->ip_id)){
            $sql = "SELECT $this->sql_cache id FROM ips WHERE ip='$ip'";
            $this->ip_id = $this->fetch_assoc_sub($sql);
            $this->ip_id = (empty($this->ip_id['id']))?0:(int)$this->ip_id['id'];
            
            //если в дочерней бд нет то делаем выборку из главной бд
            if($this->ip_id == 0){
                //$this->ip_id = $this->fetch_assoc_main($sql);
                $resourse = $this->q_main($sql);
                $this->ip_id = $this->fetch_assoc($resourse);
                /*
                //TEST                       
                file_put_contents( 'test_log_ip_id.txt' , date("Y-m-d H:i:s") .
                        " this->ip_id:  " . $this->ip_id . "\r\n", FILE_APPEND);        
                //END TEST
                */
                $this->ip_id = (empty($this->ip_id['id']))?0:(int)$this->ip_id['id'];
                
                //если в главной бд нет то делаем вставку в главную бд
                if($this->ip_id == 0){
                    $this->q_main("INSERT INTO ips (ip) VALUES ('$ip')");
                    $this->ip_id = $this->insert_id_main();
                } 
            }
            $this->mem_cache->set('ip_id_'.$ip, $this->ip_id);
        }

        //$this->_a[] = 'get ip id: '.(microtime(true) - $this->m);
    }
    
    /*
    ###устанавливает $this->os_id и $this->browser_id
    ###на основе $data->user_agent
    protected function init_os_id_and_browser_id(){

        //если переда user_agent то вынимаем из него данные
        if(!empty($this->user_agent)){
            $this->os_id = $this->getOSId($this->user_agent);
            $this->browser_id = $this->getBrowserId($this->user_agent);
        }else{
            $this->os_id = 0;
            $this->browser_id = 0;
        }
    }
    */
    
    /*
    ###на основе переданного $user_agent определяем номер OS
    ### params
    ### $user_agent - переданный в запросе цены user_agent
    ### return int - OS Id
    ###1-Win, 2-MacOS, 3-Google Android, 4 - iOS, 5 - Symbian OS, 6 - Linux, 0 - other
    function getOSId($user_agent){
        if(preg_match('/WinNT/is', $user_agent)) return 1;
        if(preg_match('/Windows NT/is', $user_agent)) return 1;
        if(preg_match('/iPhone/is', $user_agent)) return 4;
        if(preg_match('/iPad/is', $user_agent)) return 4;
        if(preg_match('/Mac OS/is', $user_agent)) return 2;
        if(preg_match('/Android/is', $user_agent)) return 3;
        if(preg_match('/SymbOS/is', $user_agent)) return 5;
        if(preg_match('/linux/is', $user_agent)) return 6;

        return 0;
    }

    ###на основе переданного $user_agent определяем номер Browser
    ### params
    ### $user_agent - переданный в запросе цены user_agent
    ### return int - browser Id 
    ###1-Win, 2-MacOS, 3-Google Android, 4 - iOS, 5 - Symbian OS, 6 - Linux, 0 - other
    function getBrowserId($user_agent){
        if(preg_match('/WinNT/is', $user_agent)) return 1;
        if(preg_match('/Windows NT/is', $user_agent)) return 1;
        if(preg_match('/iPad/is', $user_agent)) return 3;
        if(preg_match('/iPhone/is', $user_agent)) return 4;
        if(preg_match('/Mac OS/is', $user_agent)) return 2;
        if(preg_match('/Android/is', $user_agent)) return 5;

        return 0;
    }
    */
    
    ###вставляет строку о запросе цены в таблицу requests дочерней бд
    ###перезаписывает $this->request_id 
    protected function insert_request(){
          
          //AUTO_INCREMENT начинается от [номер сервера]._000000000 пример: 1000000000
          //это сделано для задания уникальности PK при сливе данных в главну бд
          $sql = "CREATE TABLE IF NOT EXISTS `requests_$this->_date` (
          `id` bigint(20) NOT NULL AUTO_INCREMENT,
          `ssp_id_int` int(11) NOT NULL,
          `country_id` int(11) NOT NULL,
          `cookie_id` int(11) NOT NULL,
          `site_id` int(11) NOT NULL,
          `path_id` int(11) NOT NULL,
          `ip_id` int(11) NOT NULL,
          `target_id` int(11) NOT NULL,
          `ads_id` int(11) NOT NULL,
          `time` datetime NOT NULL,
          `os_id` int(11) NOT NULL,
          `browser_id` int(11) NOT NULL,
          `device_id` int(11) NOT NULL,
          PRIMARY KEY (`id`),
          KEY `ads_id` (`ads_id`),
          KEY `time` (`time`)
        ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=" . $this->_server_dsp_number . "000000000";
        $this->q_sub($sql);          
          
        $sql = "INSERT INTO requests_$this->_date (ssp_id_int, country_id, cookie_id, site_id, path_id, ip_id, target_id, ads_id, time, os_id, browser_id, device_id)
        VALUES ('$this->ssp_id_int', '$this->country_id', '$this->user_id',
            '$this->site_id', '$this->path_id', '$this->ip_id',
                '".$this->banner['target_id']."', '".$this->banner['id']."',
                    NOW(), '$this->os_id', '$this->browser_id', '$this->device_id')";
        $this->q_sub($sql);

        /*
        //TEST
        file_put_contents( 'test_log_bad_auc1.txt' , date("Y-m-d H:i:s") .
                 '$sql: ' . $sql . "\r\n", FILE_APPEND);        
        //END TEST
        */
        
        $this->request_id = $this->insert_id_sub();

        //$this->_a[] = 'insert into requests: '.(microtime(true) - $this->m);

    }
    
    ###формирует и выводит ответ в json формате
    protected function echo_response(){
        
        $response = array(
            'id' => $this->banner['id'],
            'price' => $this->banner['cpm'],
            'nurl' => $this->_server_dsp.'_request/n_url.php?ads_id='.$this->banner['id'].'&price=${AUCTION_PRICE}&request_id='.$this->request_id,
            "adm"   => $this->getHtml($this->banner, $this->request_id, $this->user_id),
            'domain' => $this->banner['domain'],
            'iurl' => $this->_server_img . $this->banner['image']
        );

        echo json_encode($response);
    }
    

    ###возвращает HTML код простого баннера (изображение, флеш) или товарного
    ### params
    ### array $ads - $this->banner - массивы инициированный методом $this->select_suitable_ads_auction_and_write_stat()
    ### int $request_id - $this->request_id - id строки таблицы requests который инициируется методом $this->insert_request()
    ### int $user_id - $this->user_id - получен из запроса цены ($data->user_id)
    ### return HTML - код баннера (товарного, картинка, флеш)
    ### работает с кешем, дочерней бд
    ### может завершить скрипт
    function getHtml($ads, $request_id, $user_id = 0){
        ob_start();

        //товарный баннер
        if($ads['type_target'] == 2){
            
            /*
            $params = 'request_id='.$request_id.'&user_id='.$user_id.
                    '&ads_id='.$ads['id'].'&target_id='.$ads['target_id'].
                    '&product_template='.$ads['product_template'].
                    '&product_logo='.$ads['product_logo'].
                    '&product='.$ads['product'];
            */
            
            $params['request_id'] = $request_id;
            $params['user_id'] = $user_id;
            $params['ads_id'] = $ads['id'];
            $params['target_id'] = $ads['target_id'];
            $params['product_template'] = $ads['product_template'];
            $params['product_logo'] = $ads['product_logo'];
            $params['product'] = $ads['product'];
            
            $params = json_encode($params);
            $params = base64_encode($params);
            
            $url = $this->_server_dsp."product/index.php?$params";
            
            $_sizes_arr = array(
                0 => array('w' => 240, 'h' => 400),
                1 => array('w' => 990, 'h' => 90), 
                2 => array('w' => 728, 'h' => 90),
                3 => array('w' => 300, 'h' => 250),
                4 => array('w' => 600, 'h' => 90)
                );
            
            $html = '<iframe style="background-color:#ffffff;" width="' . $_sizes_arr[$ads['size']]['w'] . '" height="' . $_sizes_arr[$ads['size']]['h'] . '" frameborder="0" marginheight="0" marginwidth="0" scrolling="no" vspace="0" hspace="0"  src="' . $url . '"></iframe>';
            //$html = "<iframe style='background-color:#ffffff;' width='" . $_sizes_arr[$ads['size']]['w'] . "' height='" . $_sizes_arr[$ads['size']]['h'] . "' frameborder='0' marginheight='0' marginwidth='0' scrolling='no' vspace='0' hspace='0'  src='" . $url . "'></iframe>";
            
            return $html;
        }else{
            include(dirname(__FILE__) . '/ads/show.php');            
        }

        return str_replace(array("\n", "\r", "\t"),array('','',''), ob_get_clean());

    }
    
}


########################################################################
###при вызове будет запущен конструктор он выполнит все действия аукциона
########################################################################
Auction::I();