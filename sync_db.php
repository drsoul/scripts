<?php
 error_reporting(E_ALL);
 ini_set('display_errors', 'on');
 
set_error_handler('logError');

//обработчик ошибок
function logError($errno, $errstr = '', $errfile = '', $errline = ''){
    $dir = dirname(__FILE__) . '/../logs/sync_db/'; //"/home/www/dsp/public_html/logs/";//старый путь
    $file = 'sync_db_'.date('Y_m_d').'.txt';

    if($errno != 8192){
        file_put_contents($dir.$file,$errno."|$errstr|$errline\n",FILE_APPEND);
    }
}

require_once( dirname(__FILE__) . '/../db_connect.php' );

Class Sync_db extends Db_connect{    

    protected $new_control_timestamp;
    protected $_date;
    protected $_date_for_sync_requests_tb;
    protected $_date_yesterday_for_sync_requests_tb;
    public static $instance;
    
    #params
    #array $table_names_arr - массив элементы которого названия таблиц для синхронизации
    public function __construct() {        
        //подключение к базам данных в родительском классе
        parent::__construct();
        
        //установка текущей даты
        $this->_date = date('Y_m_d');
        $this->_date_yesterday = date('Y_m_d', strtotime("-1 day"));
        $this->_date_for_sync_requests_tb = date('Y-m-d');
        $this->_date_yesterday_for_sync_requests_tb = date('Y-m-d', strtotime("-1 day"));
        
        //выполнение синхронизации для полученных названий таблиц
        //$this->main_loop($table_names_arr);
        
        
    }
    
    public static function I(){
        
        if (!isset(self::$instance)){
            self::$instance = new Sync_db();
        }
        return self::$instance;
    }
    
    //производит синхронизацию таблиц
    //params
    //array $table_names_arr - массив значения элементов которого являются
    //названиями синхронизируемых таблиц
    //return в случае если передан не массив будет возвращён false
    //в процессе выполнения выводит количество модифицированных строк
    public function run($table_names_arr){
        
        if( !is_array($table_names_arr) ){
            //получен не массив
            return false;
        }
        
        $sep = '';
        $in = '';
        foreach($table_names_arr as $key => $value){
            $in .= $sep . "'" . $value . "'";
            if( empty($sep) ){
                $sep = ',';
            }
        }
        
        //берём метку времени в формате UNIX_TIMESTAMP (количество секунд с начала эпохи),
        //вместо mysql timestamp (data и time с начала эпохи)
        $sql = "SELECT $this->_sql_cache id, table_name, direction_sync, metod_sync, table_for_day, 
            UNIX_TIMESTAMP(control_timestamp) as control_unix_timestamp, test_del_row
            FROM sync_control_mark WHERE table_name IN($in)";
        
        
        
        $main_srv_time = $this->fetch_assoc_main("SELECT $this->_sql_cache UNIX_TIMESTAMP( NOW() ) as new_control_timestamp");
        //записываем новую контрольную метку времени главного сервера
        $this->new_control_timestamp = $main_srv_time['new_control_timestamp'];
        
        $resource = $this->q_sub($sql);
        while($control_marks = $this->fetch_assoc($resource)){      

            //точная перезапись обнов. строк
            if($control_marks['metod_sync'] == 1){                
                $this->exact_rewrite_updated_row($control_marks['id'],
                        $control_marks['table_name'],
                        $control_marks['control_unix_timestamp'],
                        $control_marks['test_del_row']);

            //вставка или обновление изменённых строк  
            }elseif($control_marks['metod_sync'] == 2){

                /*
                echo '<br> $value[id] - ' . $control_marks['id'];
                echo '<br> $value[table_name] - ' . $control_marks['table_name'];
                echo '<br> $value[control_unix_timestamp] - ' . $control_marks['control_unix_timestamp'];
                echo '<br> $value[table_for_day] - ' . $control_marks['table_for_day'];
                echo '<br> $value[metod_sync] - ' . $control_marks['metod_sync'];
                */
                $this->exact_rewrite_updated_row_by_sub_serv_id_and_sub_serv_row_id(
                        $control_marks['id'], $control_marks['table_name'],
                        $control_marks['control_unix_timestamp'],
                        $control_marks['table_for_day']); 
                

            //insert c фиксированными диапазонами id
            }elseif($control_marks['metod_sync'] == 3){
                $this->insert_by_fixed_range_id();                

            //обновление суммированием
            }elseif($control_marks['metod_sync'] == 4){
                /*
                echo '<br> $value[id] - ' . $control_marks['id'];
                echo '<br> $value[table_name] - ' . $control_marks['table_name'];
                echo '<br> $value[control_unix_timestamp] - ' . $control_marks['control_unix_timestamp'];
                echo '<br> $value[table_for_day] - ' . $control_marks['table_for_day'];
                echo '<br> $value[metod_sync] - ' . $control_marks['metod_sync'];
                */
                $this->update_by_summ($control_marks['id'], $control_marks['table_name']);                 
            }

        }
    }
    
    //точная перезапись обнов. строк
    //берёт строки из главной и пишет их в дочернюю
    //придостижении порционного лимата вставляет данные порциями
    //params
    //$id - id таблицы
    //$table_name - название таблицы
    //$control_timestamp - контрольная метка времени из таблицы control_mark
    //работает с таблицей контрольных меток - control_mark
    //и таблиацами переданными для синхронизации
    //обращается к главной и дочерней бд    
    protected function exact_rewrite_updated_row($id, $table_name, $control_unix_timestamp, $test_del_row = 0){
        
        //запись новой контрольной метки
        $this->update_control_timestamp($id);
        
        /*
        //выжидаем 2 секунды чтобы контрольная метка наверняка была позади момента выборки данных
        //для синхронизации, это предотвратит нарушение синхронизации
        sleep(2); //закоментил из-за того что '>' изменил на '>=' здесь: UNIX_TIMESTAMP(upd_time) >= $control_unix_timestamp
        */
        
        
        //выборка количества строк из главной бд для проверки удалённых записей
        if($test_del_row == 1){
            $sql = "SELECT $this->_sql_cache COUNT(*) as count FROM $table_name WHERE 1";
            $resource = $this->q_main($sql);
            $res = $this->fetch_assoc($resource);
            $count_rows_main = $res['count'];
        }
        
        
        //вынимаем обновлённые строки из главной бд
        $sql = "SELECT $this->_sql_cache * FROM $table_name WHERE UNIX_TIMESTAMP(upd_time) >= $control_unix_timestamp";        
        $flag_field_name_complete = 0;
        $flag_field_name_complete_1 = 0;
        $sql_fields_value = '';
        $sql_fields_name = '';
        $sql_fields_dupl_update = '';
        $loop = 0;
        $sep_row = '';
        $ids_in_for_del = '';
        
        $loop_c = 0;
        //unset($sql_fields_name_arr);
        $resource = $this->q_main($sql);
        while($res = $this->fetch_assoc($resource)){

            $sql_fields_value .= $sep_row . '(';
            
            $sep = '';
            $sep_1 = ''; 
            foreach($res as $key => $value){
                
                if($key != 'upd_time'){
                    if($flag_field_name_complete == 0){
                        //$sql_fields_name_arr[] = $key;
                        $sql_fields_name .= $sep . $key; 
                    }
                    $sql_fields_value .= $sep . "'" . $this->escape_sub($value) . "'";
                }

                //собираем значения внешних ключей строк
                if($key == 'id' OR ($table_name == 'site' AND $key == 'site_id') ){
                    $ids_in_for_del  .= $sep_row . $value;
                }else{
                    if($flag_field_name_complete_1 == 0){
                        
                        $sql_fields_dupl_update .= $sep_1 . $key . " = VALUES($key)";                         
                        $sep_1 = ',';
                    }
                }                
                
                
                $sep = ',';                
            }
            $sql_fields_value .= ')';
            //переменная $sql_fields_name собрана и дополнять её не требуется
            $flag_field_name_complete = 1;
            $flag_field_name_complete_1 = 1;
            
            $sep_row = ',';
            
            
            //вставка строк по достижению лимита для фрагмента вставки
            //если лимит не будет достинут то вставка произодёт после цикла
            //если лимит будет достигнут несколько раз то будет произведено
            //несколько вставок.
            //К тому же после лимитированных вставок после цикла ещё будет
            //вставлен остаток данных
            $loop++;
            if($loop == $this->_sync_db_insert_portion_limit){
                
                
                /*
                //удаляем в дочерней бд все строки которые
                //были изменены в главной бд
                if($table_name == 'site'){//только в таблице site PK - site_id, в других таблицах он - id
                    $id_name = 'site_id'; 
                }else{
                    $id_name = 'id';
                }
                
                $sql = "DELETE FROM $table_name WHERE $id_name IN($ids_in_for_del)";       
                $this->q_sub($sql);
                $ids_in_for_del = '';
                $id_name = '';
                
                
                //вставляем строки в дочернюю бд которые были обновлены в главной
                //вставка с идёт вместе с PK поэтому таблицы будут идентичны
                $sql = "INSERT INTO $table_name ($sql_fields_name)
                    VALUES $sql_fields_value";
                $this->q_sub($sql);
                */
                
                $sql = "INSERT INTO $table_name ($sql_fields_name)
                VALUES $sql_fields_value
                ON DUPLICATE KEY UPDATE $sql_fields_dupl_update"; //$sql_fields_name
                $this->q_sub($sql);
                
                //TEST
                //echo $sql_fields_name_without_id;
                //echo '|';
                
                $sql_fields_value = '';
                $sep_row = '';
                $sep = '';
                $sep_1 = '';
                $loop = 0;
            }
            
            
            $loop_c++;
        }        
        
        /*
        //удаление в дочерней бд строк которые были изменены в главной бд
        //либо всех, либо остатка строк если был достигнут лимит порцинной вставки
        if( !empty($ids_in_for_del) ){
            //удаляем в дочерней бд все строки которые
            //были изменены в главной бд
            if($table_name == 'site'){//только в таблице site PK - site_id, в других таблицах он - id
                $id_name = 'site_id'; 
            }else{
                $id_name = 'id';
            }
            $sql = "DELETE FROM $table_name WHERE $id_name IN($ids_in_for_del)";       
            $this->q_sub($sql);
            $ids_in_for_del = '';
            $id_name = '';
        }
        */      
        
        //вставка остатка данных, либо всех данных если небыло
        //достижения лимита для фрагмента вставки
        if( !empty($sql_fields_value) ){
            /*
            //вставляем обновлённые строки в главной бд
            $sql = "INSERT INTO $table_name ($sql_fields_name)
                    VALUES $sql_fields_value";
            $this->q_sub($sql);
            */
            
            $sql = "INSERT INTO $table_name ($sql_fields_name)
            VALUES $sql_fields_value
            ON DUPLICATE KEY UPDATE $sql_fields_dupl_update"; //$sql_fields_name_without_id
            $this->q_sub($sql);
        }
               
        
        //выборка количества строк из дочерней бд для проверки удалённых записей
        if($test_del_row == 1){
            
            if($this->main_bd == true AND $this->sub_bd == true){
                /*
                $sql = "SELECT COUNT(*) as count FROM $table_name WHERE 1";
                $resource = $this->q_sub($sql);
                $res = $this->fetch_assoc($resource);
                $count_rows_sub = $res['count'];
                */
               
                /*
                //TEST
                echo "\r\n<br><hr>Table: " . $table_name . ' $count_rows_sub: ' 
                        . $count_rows_sub . ' $count_rows_main: ' . $count_rows_main;
                //END TEST
                */
                
                /*
                //если количество строк таблицы в дочерней бд больше чем в главной
                //значит в главной таблице были удаления строк
                //нужно найти недействительные строки в таблице доч. бд и удалить их            
                if($this->get_count_rows($table_name) > $count_rows_main){//$count_rows_sub > $count_rows_main
                    if($table_name == 'site' ){                    
                        $key = 'site_id';
                    }else{
                        $key = 'id';                     
                    }


                    $sql = "SELECT MAX($key) as max_id FROM $table_name WHERE 1";
                    $resource = $this->q_sub($sql);
                    $res = $this->fetch_assoc($resource);
                    $max_id_sub = $res['max_id'];
                    
                    $sql = "SELECT MIN($key) as min_id FROM $table_name WHERE 1";
                    $resource = $this->q_sub($sql);
                    $res = $this->fetch_assoc($resource);
                    $min_id_sub = $res['min_id'];
                    

                    
                    //сравнение id порциями, в случае наличия строк с лишними id в дочерней бд
                    //удаляем их
                    $start_id = $min_id_sub - 1;//устанавливаем стартовый id чтобы не удалять начальную пустоту
                    $end_id = $min_id_sub + $this->_sync_db_delete_portion_limit; //конечный id тот же что и стартовый, но с отступом
                    $deleted_rows_summ = 0;                
                    while($end_id < $max_id_sub + $this->_sync_db_delete_portion_limit){

                        if($end_id == 0){
                            $end_id = $this->_sync_db_delete_portion_limit;
                        }

                        $sql = "SELECT $key FROM $table_name WHERE $key > $start_id AND $key <= $end_id";
                        
                        $resource = $this->q_main($sql);
                        $sep = '';
                        $in = '';
                        while( $res = $this->fetch_assoc($resource) ){
                            $in .= $sep . $res[$key];
                            $sep = ',';
                        }

                        if( empty($in) ){
                            $in_str = '';
                            //$in_str = " AND $key NOT IN(0)";
                        }else{
                            $in_str = " AND $key NOT IN($in)";
                        }

                        $sql = "DELETE FROM $table_name WHERE $key > $start_id AND $key <= $end_id
                                $in_str";
  
                        
                        $this->q_sub($sql);
                        $deleted_rows_summ += $this->affected_rows_sub();
                        
                        $start_id = $end_id;
                        $end_id = $end_id + $this->_sync_db_delete_portion_limit;

                    }  
                }
                */
                
                $this->del_nonexist_rows($table_name, $count_rows_main);
                
                //echo "\r\n Control summs: " . $this->get_count_rows($table_name) . ' ' . $count_rows_main;
                if($this->get_count_rows($table_name) != $count_rows_main){
                    //аварийная ситуация количества строк не сходятся
                    //была аварийная остановка скрипта после установки новой контрольной метки
                    #проводим полную синхронизацию таблицы
                    $this->sync_tb_full($table_name, $pk = 'id');
                    
                    $this->del_nonexist_rows($table_name, $count_rows_main);
                }
            }
        }
        
        if( empty($deleted_rows_summ) ){
            $deleted_rows_summ = 0;
        }
        
        echo "\r\n<br><hr>Table: " . $table_name . ' Sync metod: exact_rewrite_updated_row. Modificate: ' . $loop_c . ' rows. Deleted rows in table sub-db ' . $deleted_rows_summ . '. Complete.';
    }  
    
    
    protected function del_nonexist_rows($table_name, $count_rows_main){
        //если количество строк таблицы в дочерней бд больше чем в главной
        //значит в главной таблице были удаления строк
        //нужно найти недействительные строки в таблице доч. бд и удалить их   
        $count_rows_sub_test = $this->get_count_rows($table_name);
        if($count_rows_sub_test > $count_rows_main){//$count_rows_sub > $count_rows_main
            
            echo "\r\n\r\n";
            echo "Tables ($table_name) checksums do not match (sub rows: $count_rows_sub_test, main rows: $count_rows_main), run a delete nonexist rows.";
            echo "\r\n\r\n";
            
            if($table_name == 'site' ){                    
                $key = 'site_id';
            }else{
                $key = 'id';                     
            }


            $sql = "SELECT $this->_sql_cache MAX($key) as max_id FROM $table_name WHERE 1";
            $resource = $this->q_sub($sql);
            $res = $this->fetch_assoc($resource);
            $max_id_sub = $res['max_id'];

            $sql = "SELECT $this->_sql_cache MIN($key) as min_id FROM $table_name WHERE 1";
            $resource = $this->q_sub($sql);
            $res = $this->fetch_assoc($resource);
            $min_id_sub = $res['min_id'];

            
            //TEST
            echo "\r\n<br><hr>max_id_sub: " . $max_id_sub;
            echo "\r\n<br><hr>min_id_sub: " . $min_id_sub;
            echo "\r\n";
            //END TEST
            

            //сравнение id порциями, в случае наличия строк с лишними id в дочерней бд
            //удаляем их
            $start_id = $min_id_sub - 1;//устанавливаем стартовый id чтобы не удалять начальную пустоту
            $end_id = $min_id_sub + $this->_sync_db_delete_portion_limit; //конечный id тот же что и стартовый, но с отступом
            $deleted_rows_summ = 0; 
            $deleted_rows = 0;
            while($end_id < $max_id_sub + $this->_sync_db_delete_portion_limit){

                if($end_id == 0){
                    $end_id = $this->_sync_db_delete_portion_limit;
                }

                $sql = "SELECT $this->_sql_cache $key FROM $table_name WHERE $key > $start_id AND $key <= $end_id";

                /*
                //TEST
                echo "\r\n<br><hr>$sql";
                //END TEST
                */

                $resource = $this->q_main($sql);
                
                //if($this->num_rows($resource) > 0){
                    
                    $sep = '';
                    $in = '';
                    while( $res = $this->fetch_assoc($resource) ){
                        $in .= $sep . $res[$key];
                        $sep = ',';
                    }

                    if( empty($in) ){
                        $in_str = '';
                        //$in_str = " AND $key NOT IN(0)";
                    }else{
                        $in_str = " AND $key NOT IN($in)";
                    }

                    $sql = "DELETE FROM $table_name WHERE $key > $start_id AND $key <= $end_id
                            $in_str";

                    
                    //TEST
                    //echo "\r\n<br><hr>$sql";
                    //END TEST
                    

                    $this->q_sub($sql);
                    $deleted_rows = $this->affected_rows_sub();
                    $deleted_rows_summ += $this->affected_rows_sub();
                //}
                
                //TEST
                echo ":$deleted_rows";
                //echo "\r\n<br><hr>deleted_rows: $deleted_rows";
                //END TEST
                

                $start_id = $end_id;
                $end_id = $end_id + $this->_sync_db_delete_portion_limit;

            }    
            
            //TEST
            echo "\r\n<br><hr>$deleted_rows_summ: $deleted_rows_summ";
            //END TEST

            echo "\r\n\r\n";
            echo "Tables ($table_name), end delete nonexist rows.";
            echo "\r\n\r\n";
        }
    }
    
    
    //делает полную синхронизацию таблицы
    //долго, но точно
    protected function sync_tb_full($table_name, $pk = 'id'){
        
        echo "\r\n\r\n";
        echo "Tables ($table_name) checksums do not match, run a full synchronization.";
        echo "\r\n\r\n";
        
        $min_id = $this->get_min_id($table_name, $pk, $db = 'main');
        $max_id = $this->get_max_id($table_name, $pk, $db = 'main');
        
        //$limit = $min_id;
        $start_id = $min_id;
        $end_id = $min_id + $this->_sync_db_insert_portion_limit;
        
        while($max_id > $start_id){
            

                    

            //вынимаем строки из главной бд
            $sql = "SELECT $this->_sql_cache * FROM $table_name WHERE $pk >= $start_id AND $pk <= $end_id"; 
            
            echo "\r\n" . '$sql: ' . $sql . "\r\n";
            
            $resource = $this->q_main($sql);
            
            $num_rows = $this->num_rows($resource);
            if($num_rows > 0){
                //$res = $this->fetch_assoc($resource);
                //$sql_pre = "INSERT INTO $table_name VALUES ";

                $sql_values_all = '';
                $sql_names = '';
                $sep = '';
                $sep_1 = '';
                $sep_2 = '';
                $flag_sql_names_create = 0;
                $sql_on_duplicate = '';
                while($res = $this->fetch_assoc($resource)){

                    $sql_values_1_req = '';
                    foreach($res as $key => $value){
                        $sql_values_1_req .= $sep . "'" . $this->escape_sub($value) . "'";

                        if($flag_sql_names_create == 0){
                            $sql_names .= $sep . $key;
                            $sql_on_duplicate .= $sep . " $key=VALUES($key)";
                        }
                        $sep = ',';
                    }
                    $flag_sql_names_create = 1;
                    $sep = '';

                    $sql_values_all .= $sep_1 .  '(' . $sql_values_1_req . ')';
                    $sep_1 = ',';


                }

                $sql = "INSERT INTO $table_name ($sql_names) VALUES $sql_values_all ON DUPLICATE KEY UPDATE $sql_on_duplicate";

                //echo "/r/n" . '$sql: ' . $sql;
                //echo "/r/n limit" . $max_id . ' ' . $limit;
                if( !empty($sql_values_all) ){
                    $this->q_sub($sql);
                }
                

                echo "\r\n" . "max_id: $max_id, start_id: $start_id, end_id: $end_id" . ' $this->num_rows($resource): ' . "$num_rows" . ' $resource: ' . "$resource";
                

            }else{
                //отображает индикатор того что пошли пустые места в индексах таблицы.
                echo '-';
            }
            
            $start_id = $start_id + $this->_sync_db_insert_portion_limit;
            $end_id = $end_id + $this->_sync_db_insert_portion_limit;
            
        }
        
        echo "\r\n\r\n";
        echo "Tables ($table_name) full synchronization end.";
        echo "\r\n\r\n";
    }
    
    //params 
    //$db - sub | main
    //$table_name - название таблицы
    protected function get_count_rows($table_name, $db = 'sub'){
        $sql = "SELECT $this->_sql_cache COUNT(*) as count FROM $table_name WHERE 1";
        if($db == 'sub'){
            $resource = $this->q_sub($sql);
        }elseif($db == 'main'){
            $resource = $this->q_main($sql);
        }
        $res = $this->fetch_assoc($resource);
        return $res['count'];
    }
    
    //params 
    //$db - sub | main
    //$pk - название первичного ключа
    //$table_name - название таблицы
    protected function get_max_id($table_name, $pk = 'id', $db = 'sub'){
        $sql = "SELECT $this->_sql_cache MAX($pk) as max FROM $table_name WHERE 1"; 
        if($db == 'sub'){
            $resource = $this->q_sub($sql);
        }elseif($db == 'main'){
            $resource = $this->q_main($sql);
        }
        $res = $this->fetch_assoc($resource);
        return $res['max'];
    }

    //params 
    //$db - sub | main
    //$pk - название первичного ключа
    //$table_name - название таблицы
    protected function get_min_id($table_name, $pk = 'id', $db = 'sub'){
        $sql = "SELECT $this->_sql_cache MIN($pk) as min FROM $table_name WHERE 1";        
        if($db == 'sub'){
            $resource = $this->q_sub($sql);
        }elseif($db == 'main'){
            $resource = $this->q_main($sql);
        }
        $res = $this->fetch_assoc($resource);
        return $res['min'];
    }

    //вставка или обновление изменённых строк - для таблицы views_$this->_date
    //берёт строки из дочерней и пишет их в главную
    //придостижении порционного лимата вставляет данные порциями
    //params
    //$id - id таблицы
    //$table_name - название таблицы
    //$control_timestamp - контрольная метка времени из таблицы control_mark
    //работает с таблицей контрольных меток - control_mark
    //и таблиацами переданными для синхронизации
    //обращается к главной и дочерней бд    
    //внимание - метод заточен под 1 таблицу, при необходимости воспользоваться им для других таблиц
    //его нужно адаптировать
    //ВСТАВЛЯЕМЫЕ В БД ЗНАЧЕНИЯ НЕ ЭКРАНИРУЮТСЯ
    public function exact_rewrite_updated_row_by_sub_serv_id_and_sub_serv_row_id(
            $id, $table_name, $control_unix_timestamp, $table_for_day){
        
        //запись новой контрольной метки
        $this->update_control_timestamp($id);   
        
        /*
        //выжидаем 2 секунды чтобы контрольная метка наверняка была позади момента выборки данных
        //для синхронизации, это предотвратит нарушение синхронизации
        sleep(2);//закоментил из-за того что '>' изменил на '>=' здесь: UNIX_TIMESTAMP(upd_time) >= $control_unix_timestamp
        */
        
        if($table_for_day == 1){
            $table_name = $table_name . '_'. $this->_date;
            
            /*
            $sql ="
            CREATE TABLE IF NOT EXISTS `views_$this->_date` (
              `id` bigint(20) NOT NULL AUTO_INCREMENT,
              `site_id` int(11) NOT NULL,
              `path_id` bigint(20) NOT NULL,
              `time` datetime NOT NULL,
              `click` tinyint(1) NOT NULL DEFAULT '0',
              `ads_id` int(11) NOT NULL,
              `dsp_bid_request_id` bigint(20) NOT NULL,
              `ip_id` int(11) NOT NULL,
              `expense` float NOT NULL,
              `expense_ssp` float NOT NULL,
              `bill` tinyint(4) NOT NULL,
              `ssp_id_int` int(11) NOT NULL,
              `target_id` int(11) NOT NULL,
              `cookie_id` int(11) NOT NULL,
              `country_id` int(11) NOT NULL,
              `sub_serv_id` int(3) NOT NULL DEFAULT '0',
              `sub_serv_row_id` int(15) NOT NULL DEFAULT '0',
              PRIMARY KEY (`id`),
              UNIQUE KEY `uniq_row_on_sub_srv` (`sub_serv_id`,`sub_serv_row_id`),
              KEY `dsp_bid_request_id` (`dsp_bid_request_id`),
              KEY `ads_id` (`ads_id`),
              KEY `target_id` (`target_id`),
              KEY `country_id` (`country_id`),
              KEY `ip_id` (`ip_id`),
              KEY `path_id` (`path_id`),
              KEY `site_id` (`site_id`)
            ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
            */
            
            //создание таблицы в главной бд
            $sql ="
            CREATE TABLE IF NOT EXISTS `views_$this->_date` (
              `id` bigint(20) NOT NULL AUTO_INCREMENT,
              `site_id` int(11) NOT NULL,
              `path_id` bigint(20) NOT NULL,
              `time` datetime NOT NULL,
              `click` tinyint(1) NOT NULL DEFAULT '0',
              `ads_id` int(11) NOT NULL,
              `dsp_bid_request_id` bigint(20) NOT NULL,
              `ip_id` int(11) NOT NULL,
              `expense` float NOT NULL,
              `expense_ssp` float NOT NULL,
              `bill` tinyint(4) NOT NULL,
              `ssp_id_int` int(11) NOT NULL,
              `target_id` int(11) NOT NULL,
              `cookie_id` int(11) NOT NULL,
              `country_id` int(11) NOT NULL,
              `sub_serv_id` int(3) NOT NULL DEFAULT '0',
              `sub_serv_row_id` int(15) NOT NULL DEFAULT '0',
              `upd_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
              PRIMARY KEY (`id`),              
              KEY `dsp_bid_request_id` (`dsp_bid_request_id`),
              KEY `ads_id` (`ads_id`),
              KEY `target_id` (`target_id`),
              KEY `country_id` (`country_id`),
              KEY `ip_id` (`ip_id`),
              KEY `path_id` (`path_id`),
              KEY `site_id` (`site_id`),
              KEY `time` (`time`)
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
            //UNIQUE KEY `uniq_row_on_sub_srv` (`sub_serv_id`,`sub_serv_row_id`), //17.06.14
            $this->q_main($sql);            
    
            /*
            $sql ="
            CREATE TABLE IF NOT EXISTS `views_$this->_date` (
              `id` bigint(20) NOT NULL AUTO_INCREMENT,
              `site_id` int(11) NOT NULL,
              `path_id` bigint(20) NOT NULL,
              `time` datetime NOT NULL,
              `click` tinyint(1) NOT NULL DEFAULT '0',
              `ads_id` int(11) NOT NULL,
              `dsp_bid_request_id` bigint(20) NOT NULL,
              `ip_id` int(11) NOT NULL,
              `expense` float NOT NULL,
              `expense_ssp` float NOT NULL,
              `bill` tinyint(4) NOT NULL,
              `ssp_id_int` int(11) NOT NULL,
              `target_id` int(11) NOT NULL,
              `cookie_id` int(11) NOT NULL,
              `country_id` int(11) NOT NULL,
              `sub_serv_id` int(3) NOT NULL DEFAULT '0',
              `sub_serv_row_id` int(15) NOT NULL DEFAULT '0',
              `upd_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
              PRIMARY KEY (`id`),
              UNIQUE KEY `uniq_row_on_sub_srv` (`sub_serv_id`,`sub_serv_row_id`),
              KEY `dsp_bid_request_id` (`dsp_bid_request_id`),
              KEY `ads_id` (`ads_id`),
              KEY `target_id` (`target_id`),
              KEY `country_id` (`country_id`),
              KEY `ip_id` (`ip_id`),
              KEY `path_id` (`path_id`),
              KEY `site_id` (`site_id`)
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
            */
            
            //создание таблицы в дочерней бд (таблицы в главной и дочерней бд разные)
            $sql ="
            CREATE TABLE IF NOT EXISTS `views_$this->_date` (
              `id` bigint(20) NOT NULL AUTO_INCREMENT,
              `site_id` int(11) NOT NULL,
              `path_id` bigint(20) NOT NULL,
              `time` datetime NOT NULL,
              `click` tinyint(1) NOT NULL DEFAULT '0',
              `ads_id` int(11) NOT NULL,
              `dsp_bid_request_id` bigint(20) NOT NULL,
              `ip_id` int(11) NOT NULL,
              `expense` float NOT NULL,
              `expense_ssp` float NOT NULL,
              `bill` tinyint(4) NOT NULL,
              `ssp_id_int` int(11) NOT NULL,
              `target_id` int(11) NOT NULL,
              `cookie_id` int(11) NOT NULL,
              `country_id` int(11) NOT NULL,
              `upd_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
              PRIMARY KEY (`id`),
              KEY `dsp_bid_request_id` (`dsp_bid_request_id`),
              KEY `ads_id` (`ads_id`),
              KEY `target_id` (`target_id`),
              KEY `country_id` (`country_id`),
              KEY `ip_id` (`ip_id`),
              KEY `path_id` (`path_id`),
              KEY `site_id` (`site_id`),
              KEY `time` (`time`)
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
            
            $this->q_sub($sql);
        }
        
        //вынимаем обновлённые строки из главной бд
        $sql = "SELECT $this->_sql_cache * FROM $table_name WHERE UNIX_TIMESTAMP(upd_time) >= $control_unix_timestamp"; 
        $this->sync_views($sql, $table_name);
        
        //синхронизация остатков в предыдущий день
        $this->residue_procession_views();
        
    }
    
    
    //синхронизирует таблицу показов
    protected function sync_views($sql, $table_name){
        
        $flag_field_name_complete = 0;
        $sql_fields_value = '';
        $sql_fields_name = '';
        $loop = 0;
        $sep_row = '';
        $loop_c = 0;
        
        //{$db_srv} направляет запрос в случае простой синхронизации на дочерний сервер,
        // а в случае синхронизации остатотков на главный сервер
        $resource = $this->q_sub($sql);
        while($res = $this->fetch_assoc($resource)){       
            
            $sql_fields_value .= $sep_row . '(';
            $sep_row = ',';
            $sep = '';
            foreach($res as $key => $value){
                
                //т.к. нам не нужно вставлять id строки дочерней бд в главную,
                //вместо этого мы занесём дочерний id в специальное поле - sub_serv_row_id
                //а id в главной бд будет происвоен автоматически
                if($key != 'id' AND $key != 'upd_time'){
                    if($flag_field_name_complete == 0){
                        $sql_fields_name .= $sep . $key;   
                    }
                    $sql_fields_value .= $sep . "'" . $value . "'";
                }elseif($key == 'id'){
                    if($flag_field_name_complete == 0){
                        $sql_fields_name .= $sep . "sub_serv_id,sub_serv_row_id";
                    }
                    $sql_fields_value .= $sep . "'$this->_server_dsp_number','$value'";
                }
                
                if( empty($sep) ){
                    $sep = ',';
                }
                
            }
            $sql_fields_value .= ')';
            
            //переменная $sql_fields_name собрана и дополнять её не требуется
            $flag_field_name_complete = 1;            
            
            //вставка строк по достижению лимита для фрагмента вставки
            //если лимит не будет достинут то вставка произодёт после цикла
            //если лимит будет достигнут несколько раз то будет произведено
            //несколько вставок.
            //К тому же после лимитированных вставок после цикла ещё будет
            //вставлен остаток данных
            $loop++;
            if($loop == $this->_sync_db_insert_portion_limit){
                //вставляем обновлённые строки в главной бд
                //mass insert or update
                $sql = "INSERT INTO $table_name ($sql_fields_name)
                    VALUES $sql_fields_value
                    ON DUPLICATE KEY UPDATE click=VALUES(click)"; 
                $this->q_main($sql);
                
                $sql_fields_value = '';
                $sep_row = '';
                $sep = '';
                $loop = 0;
            }
            
            $loop_c++;
        }
        
        //вставка остатка данных, либо всех данных если небыло
        //достижения лимита для фрагмента вставки
        if( !empty($sql_fields_value) ){
            //вставляем строки в главную бд которые были обновлены в дочерней бд           
            //mass insert or update
            $sql = "INSERT INTO $table_name ($sql_fields_name)
                VALUES $sql_fields_value
                ON DUPLICATE KEY UPDATE click=VALUES(click)"; 
            $this->q_main($sql);
            
        }        
        
        echo "\r\n<br><hr>Table: " . $table_name . ' Sync metod: exact_rewrite_updated_row_by_sub_serv_id_and_sub_serv_row_id. Modificate: ' . $loop_c . ' rows. Complete.';        
        
    }
    
    //синхронизирует остаток предыдущего дня для таблиц показов
    protected function residue_procession_views(){
        
        $sql = "SELECT $this->_sql_cache * FROM sync_requests_tb WHERE date='$this->_date_yesterday_for_sync_requests_tb'";  
        $sync_level_yesterday = $this->fetch_assoc_sub($sql);
        
        /*
        //TEST
        echo '$this->_date_yesterday_for_sync_requests_tb: ' . $this->_date_yesterday_for_sync_requests_tb . "\r\n";
        echo '$sync_level_yesterday[residue_processed]: ' . $sync_level_yesterday['residue_processed'] . "\r\n";
        echo '$sync_level_yesterday[id]: ' . $sync_level_yesterday['id'] . "\r\n";
        echo '$sync_level_yesterday[date]: ' . $sync_level_yesterday['date'] . "\r\n";
        echo '$sync_level_yesterday[level]: ' . $sync_level_yesterday['level'] . "\r\n";
        */
        
        //если остаток ещё не был синхронизирован то делаем это
        if( $sync_level_yesterday['residue_processed_views'] == 0 ){
            //$this->sync_request_tb($this->_date_yesterday_for_sync_requests_tb, $this->_date_yesterday);
            $sql = "SELECT $this->_sql_cache MAX( sub_serv_row_id ) as sub_serv_row_id_max FROM views_$this->_date_yesterday WHERE sub_serv_id = $this->_server_dsp_number";
            $sub_serv_row_id = $this->fetch_assoc_main($sql);
            $sub_serv_row_id = $sub_serv_row_id['sub_serv_row_id_max'];
            
            //TEST
            //echo "\r\n";
            //echo 'sync residue_procession_views';
            
            $sql = "SELECT $this->_sql_cache * FROM views_$this->_date_yesterday WHERE id > $sub_serv_row_id"; 
            $this->sync_views($sql, "views_$this->_date_yesterday");       
            
            $sql = "UPDATE sync_requests_tb SET residue_processed_views = 1 WHERE id = " . $sync_level_yesterday['id'];  
            $this->q_sub($sql);
        }  
        
    }
    
    //синхронизация таблицы requests_$this->_date
    //внимание метод заточен под 1 таблицу,
    //при необходимости синхронизации других таблиц этим методом его нужно адаптировать
    //ВСТАВЛЯЕМЫЕ В БД ЗНАЧЕНИЯ НЕ ЭКРАНИРУЮТСЯ
    public function insert_by_fixed_range_id(){
        
        //создаём таблицу в главной бд
         $sql = "CREATE TABLE IF NOT EXISTS `requests_$this->_date` (
          `id` bigint(20) NOT NULL AUTO_INCREMENT,
          `ssp_id_int` int(11) NOT NULL,
          `country_id` int(11) NOT NULL,
          `cookie_id` int(11) NOT NULL,
          `site_id` int(11) NOT NULL,
          `path_id` int(11) NOT NULL,
          `ip_id` int(11) NOT NULL,
          `target_id` int(11) NOT NULL,
          `ads_id` int(11) NOT NULL,
          `time` datetime NOT NULL,
          `os_id` int(11) NOT NULL,
          `browser_id` int(11) NOT NULL,
          `device_id` int(11) NOT NULL,
          PRIMARY KEY (`id`),
          KEY `ads_id` (`ads_id`),
          KEY `time` (`time`)
        ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
         
        $this->q_main($sql); 
        
        
        //создаём таблицу в дочерней бд
         $sql = "CREATE TABLE IF NOT EXISTS `requests_$this->_date` (
          `id` bigint(20) NOT NULL AUTO_INCREMENT,
          `ssp_id_int` int(11) NOT NULL,
          `country_id` int(11) NOT NULL,
          `cookie_id` int(11) NOT NULL,
          `site_id` int(11) NOT NULL,
          `path_id` int(11) NOT NULL,
          `ip_id` int(11) NOT NULL,
          `target_id` int(11) NOT NULL,
          `ads_id` int(11) NOT NULL,
          `time` datetime NOT NULL,
          `os_id` int(11) NOT NULL,
          `browser_id` int(11) NOT NULL,
          `device_id` int(11) NOT NULL,
          PRIMARY KEY (`id`),
          KEY `ads_id` (`ads_id`),
          KEY `time` (`time`)
        ) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=" . $this->_server_dsp_number . "000000000 ;";

        $this->q_sub($sql);
        
        //синхронизация остатка предыдущего дня
        $sql = "SELECT $this->_sql_cache * FROM sync_requests_tb WHERE date='$this->_date_yesterday_for_sync_requests_tb'";  
        $sync_level_yesterday = $this->fetch_assoc_sub($sql);
        
        /*
        //TEST
        echo '$this->_date_yesterday_for_sync_requests_tb: ' . $this->_date_yesterday_for_sync_requests_tb . "\r\n";
        echo '$sync_level_yesterday[residue_processed]: ' . $sync_level_yesterday['residue_processed'] . "\r\n";
        echo '$sync_level_yesterday[id]: ' . $sync_level_yesterday['id'] . "\r\n";
        echo '$sync_level_yesterday[date]: ' . $sync_level_yesterday['date'] . "\r\n";
        echo '$sync_level_yesterday[level]: ' . $sync_level_yesterday['level'] . "\r\n";
        */
        
        //если остаток ещё не был синхронизирован то делаем это
        if( $sync_level_yesterday['residue_processed'] == 0 ){
            $this->sync_request_tb($this->_date_yesterday_for_sync_requests_tb, $this->_date_yesterday);
            
            $sql = "UPDATE sync_requests_tb SET residue_processed = 1 WHERE id = " . $sync_level_yesterday['id'];  
            $this->q_sub($sql);
        }        
        
        //синхронизация текущего дня
        $this->sync_request_tb($this->_date_for_sync_requests_tb, $this->_date);
        
    }
    
    
    protected function sync_request_tb($date_for_sync_requests_tb, $_date_for_requests){
                //вынимаем дневной уровень синхронизации
        $sql = "SELECT $this->_sql_cache * FROM sync_requests_tb WHERE date='$date_for_sync_requests_tb'";  
        $sync_level = $this->fetch_assoc_sub($sql);  
        
        //если уровня синхронизации нет то копируем все строки
        if( empty($sync_level['level']) OR $sync_level['level'] == 0 ){
            //отсортируем запрос от большего id к меньшему чтобы первым был новый уровень
            $sql = "SELECT $this->_sql_cache * FROM requests_$_date_for_requests WHERE 1 ORDER BY id DESC";   
            
            $return = $this->sync_requests($sql, $_date_for_requests);
            
            /*
            //TEST
            echo 'level empty' . "\r\n";
            echo '$sql: ' . $sql . "\r\n";
            echo '$date_for_sync_requests_tb: ' . $date_for_sync_requests_tb . "\r\n";
            echo '$_date_for_requests: ' . $_date_for_requests . "\r\n";
            echo '$sync_level[level]: ' . $sync_level['level'] . "\r\n";
            echo '$return[new_level]: ' . $return['new_level'] . "\r\n";
            echo '$return[modificate_rows]: ' . $return['modificate_rows'] . "\r\n";
            */
            
            $this->insert_new_level_sync_requests_tb($return['new_level'], $date_for_sync_requests_tb);
        
        //если уронь синхронизации есть то копируем строки выше него
        }else{            
            $sql = "SELECT $this->_sql_cache * FROM requests_$_date_for_requests WHERE id > " . $sync_level['level'] . "  ORDER BY id DESC";
            
            $return = $this->sync_requests($sql, $_date_for_requests);
            
            /*
            //TEST
            echo 'level isset' . "\r\n";
            echo '$sql: ' . $sql . "\r\n";
            echo '$date_for_sync_requests_tb: ' . $date_for_sync_requests_tb . "\r\n";
            echo '$_date_for_requests: ' . $_date_for_requests . "\r\n";
            echo '$sync_level[level]: ' . $sync_level['level'] . "\r\n";
            echo '$return[new_level]: ' . $return['new_level'] . "\r\n";
            echo '$return[modificate_rows]: ' . $return['modificate_rows'] . "\r\n";
            */
            
            $this->insert_new_level_sync_requests_tb($return['new_level'], $date_for_sync_requests_tb);
        }
        
        echo "\r\n<br><hr>Table: requests_$_date_for_requests Sync metod: insert_by_fixed_range_id. Modificate: " . $return['modificate_rows'] . ' rows. Complete.';
    }
    
    //производит синхронизацию таблицы requests
    //params
    //$sql - sql запроса на выборка строк из requests_$this->_date
    //return int $new_level - новый уровень синхронизации
    protected function sync_requests($sql, $_date_for_requests){
        
        $flag = 1;
        $new_level = 0;
        $loop = 0;

        $flag_field_name_complete = 0;
        $sql_fields_value = '';
        $sql_fields_name = '';
        $sep_row = '';
        
        //цикл по всем строкам дочерней бд
        $resource = $this->q_sub($sql);   
        $loop_c = 0;
        while($res = $this->fetch_assoc($resource) ){
            if($flag == 1){
                //записываем новый уровень (максимальный id в момент синхронизации)
                $new_level = $res['id'];
                $flag = 0;
            }

            $sql_fields_value .= $sep_row . '(';
            $sep_row = ',';
            $sep = '';
            //цикл по всем элементам строки дочерней бд
            foreach($res as $key => $value){
                if($flag_field_name_complete == 0){
                    //$sql_fields_name_arr[] = $key;
                    $sql_fields_name .= $sep . $key; 
                }
                $sql_fields_value .= $sep . "'" . $value . "'";

                if( empty($sep) ){
                    $sep = ',';
                }
            }
            $sql_fields_value .= ')';

            //переменная $sql_fields_name собрана и дополнять её не требуется
            $flag_field_name_complete = 1;

            $loop++;
            //фрагментарная вставка
            if($loop == $this->_sync_db_insert_portion_limit){
                //вставляем новые строки в главную бд
                //mass insert ignore
                $sql = "INSERT IGNORE INTO requests_$_date_for_requests ($sql_fields_name)
                    VALUES $sql_fields_value"; 
                $this->q_main($sql);

                $sql_fields_value = '';
                $sep_row = '';
                $sep = '';
                $loop = 0;
            }
            
            $loop_c++;
        }

        //остаточная вставка
        if( !empty($sql_fields_value) ){
            //вставляем строки в главную бд которые были добавлены в дочерней бд           
            //mass insert ignore
            $sql = "INSERT IGNORE INTO requests_$_date_for_requests ($sql_fields_name)
                VALUES $sql_fields_value"; 
            $this->q_main($sql);

        }            
        
        $return['new_level'] = $new_level;
        $return['modificate_rows'] = $loop_c;
        return $return;
    }
    
    //синхронизация методом суммирования
    //внимание - методо заточен под определённые таблицы,
    //при необходимости синхронизировать другие таблицы этим методом
    //нужно дописать обновляемые поля для таблицы в методе
    //ВСТАВЛЯЕМЫЕ В БД ЗНАЧЕНИЯ НЕ ЭКРАНИРУЮТСЯ
    protected function update_by_summ($id, $table_name){

        //задаём обновляемые поля для таблицы
        unset($updating_fields);
        if($table_name == 'day_stat'){
            $updating_fields[] = 'bids';
            $updating_fields[] = 'views';
            $updating_fields[] = 'clicks';
            $updating_fields[] = 'price';
        }elseif($table_name == 'target_stats'){
            $updating_fields[] = 'requests';
            $updating_fields[] = 'views';
        }elseif($table_name == 'day_stats_hour'){
            $updating_fields[] = 'bids';
            $updating_fields[] = 'views';
            $updating_fields[] = 'clicks';
            $updating_fields[] = 'price';
        }elseif($table_name == 'category_day_stat'){
            $updating_fields[] = 'count';
        }elseif($table_name == 'day_stat_country'){
            $updating_fields[] = 'count';
        }elseif($table_name == 'ssp_banner_day_stat'){
            $updating_fields[] = 'count';
        }elseif($table_name == 'banner_browsers_stat'){
            $updating_fields[] = 'count';
        }elseif($table_name == 'banner_os_stat'){
            $updating_fields[] = 'count';
        }elseif($table_name == 'money_history'){
            $updating_fields[] = 'sum';
        }elseif($table_name == 'day_stats_domains'){
            $updating_fields[] = 'views';
            $updating_fields[] = 'clicks';
            $updating_fields[] = 'price';
        }elseif($table_name == 'site_stats'){
            $updating_fields[] = 'views';
            $updating_fields[] = 'clicks';
            $updating_fields[] = 'price';
        }elseif($table_name == 'ssp_day_stat'){
            $updating_fields[] = 'bids';
            $updating_fields[] = 'views';
            $updating_fields[] = 'clicks';
            $updating_fields[] = 'price';
        }elseif($table_name == 'banner_device_stat'){
            $updating_fields[] = 'count';
        }        


        $or = '';
        $sep = '';
        $there = '';
        $update_new_old = '';
        $update_sync = '';
        foreach($updating_fields as $key => $value){
            //собираем условие для sql, пример: bids != bids_old OR views != views_old
            $there .= $or . $table_name . '.' . $value . ' != ' . $table_name . '.' . $value . '_old ';
            $or = 'OR ';
            
            
            //собираем обновляемые поля для вставки новых old значений полей
            //$update - используется ниже
            $update_new_old .= $sep . $value . '_old ' . ' = ' . "VALUES($value)";
           
            
            //собираем обновляемые поля для заливки в главную базу
            $update_sync .= $sep . $value . ' = ' . "$value + VALUES($value)";
            $sep = ', ';
            
        }       
        
        //echo '<hr><br> $there - ' . $there;
        //echo '<hr><br> $update_new_old - ' . $update_new_old;
        //echo '<hr><br> $update_sync - ' . $update_sync;

        //берём все изменённые строки
        $sql = "SELECT $this->_sql_cache * FROM $table_name WHERE $there";

        $flag_field_name_complete = 0;
        $flag_field_name_complete_1 = 0;
        $sql_fields_value = '';
        $sql_fields_value_1 = '';
        $sql_fields_name = '';
        $sql_fields_name_1 = '';
        $loop = 0;
        $sep_row = '';
        $sep_row_1 = '';
        $ids_in_for_del = '';

        $resource = $this->q_sub($sql);

        $loop_c = 0;
        while($res = $this->fetch_assoc($resource) ){
            
            //echo '<hr><br> $res - ' . var_dump($res);
            
            $sql_fields_value .= $sep_row . '(';
            $sql_fields_value_1 .= $sep_row_1 . '(';

            $sep = '';
            $sep_1 = '';
            $sep_2 = '';
            foreach($res as $key => $value){

                //собираем строку названий полей для вставки old значений
                //в туже бд из которой брали строки
                if($flag_field_name_complete == 0){                            
                    $sql_fields_name .= $sep . $key; 
                }                        

                //нам не нужны old поля так как их нет в главной бд
                //также проверяем чтобы не попались поля суммирования их мы обработаем отдельно ниже
                if( strpos($key, '_old') === false AND !in_array($key, $updating_fields) ){
                    //собираем строку названий полей для вставки в главную бд
                    if($flag_field_name_complete_1 == 0){
                        $sql_fields_name_1 .= $sep_1 . $key;
                    }

                    if($key != 'id'){
                        //собираем строки значений для вставки в главную бд
                        $sql_fields_value_1 .= $sep_2 . "'" . $value . "'";
                    }else{
                        $sql_fields_value_1 .= $sep_2 . 'NULL';
                    }
                }
                
                //вычитание из нового значения старого для последующего суммирования с соответствующим
                //значением в главной бд
                if( strpos($key, '_old') !== false ){
                    $key_new = str_replace('_old', '',$key);
                    
                    if($flag_field_name_complete_1 == 0){
                        $sql_fields_name_1 .= $sep_1 . $key_new;
                    }

                    $value_for_main_summ = $res[$key_new] - $value;
                    $sql_fields_value_1 .= $sep_2 . "'$value_for_main_summ'";//" . $res[$key_new] - $value . "
                }

                $sql_fields_value .= $sep . "'" . $value . "'";

                 
                $sep = ',';   
                $sep_1 = ',';
                $sep_2 = ',';
            }
            $sql_fields_value .= ')';
            $sql_fields_value_1 .= ')';
            //переменная $sql_fields_name собрана и дополнять её не требуется
            $flag_field_name_complete = 1;
            $flag_field_name_complete_1 = 1;

            $sep_row = ',';
            $sep_row_1 = ',';
            
            //echo '<hr><br>$sql_fields_value_1 - ' . $sql_fields_value_1;
            
            //вставка строк по достижению лимита для фрагмента вставки
            //если лимит не будет достинут то вставка произодёт после цикла
            //если лимит будет достигнут несколько раз то будет произведено
            //несколько вставок.
            //К тому же после лимитированных вставок после цикла ещё будет
            //вставлен остаток данных
            $loop++;
            if($loop == $this->_sync_db_insert_portion_limit){ //$this->_sync_db_insert_portion_limit

                //обновление old полей
                $sql = "INSERT INTO $table_name ($sql_fields_name)
                    VALUES $sql_fields_value
                    ON DUPLICATE KEY UPDATE $update_new_old"; 
                $this->q_sub($sql);

                $sql_fields_value = '';
                $sep_row = '';
                $sep = '';
                $loop = 0;
                //echo '<hr><br> q_sub portion';

                //заливка данных в главную бд
                $sql = "INSERT INTO $table_name ($sql_fields_name_1)
                    VALUES $sql_fields_value_1
                    ON DUPLICATE KEY UPDATE $update_sync"; 
                $this->q_main($sql);

                $sql_fields_value_1 = '';
                $sep_row_1 = '';
                $sep_1 = '';
                $loop = 0;
                
                //echo '<hr><br> q_main portion';
            }
            
            $loop_c++;
        }

        //обновление old полей
        //вставка остатка данных, либо всех данных если небыло
        //достижения лимита для фрагмента вставки
        if( !empty($sql_fields_value) ){
            //записываем новые old значения полей
            $sql = "INSERT INTO $table_name ($sql_fields_name)
                VALUES $sql_fields_value
                ON DUPLICATE KEY UPDATE $update_new_old"; 
            //echo "\r\n" . '<br><hr><br> $sql_fields_value ' . $sql;
            $this->q_sub($sql);
            
            //echo '<hr><br>q_sub old' . $sql;
        }

        //заливка данных в главную бд
        //вставка остатка данных, либо всех данных если небыло
        //достижения лимита для фрагмента вставки
        if( !empty($sql_fields_value_1) ){
            //записываем новые old значения полей
            $sql = "INSERT INTO $table_name ($sql_fields_name_1)
                VALUES $sql_fields_value_1
                ON DUPLICATE KEY UPDATE $update_sync"; 
            //echo "\r\n" . '<br><hr><br> $sql_fields_value_1 '. $sql;
            $this->q_main($sql);
            
            //echo '<hr><br>q_main' . $sql;
        } 
        
        echo "\r\n<br><hr>Table: " . $table_name . ' Sync metod: update_by_summ. Modificate: ' . $loop_c . ' rows. Complete.';
    }
    
    protected function insert_new_level_sync_requests_tb($new_level, $date_for_sync_requests_tb){
        
        $row_sync_req_tb = $this->fetch_assoc( $this->q_sub("SELECT $this->_sql_cache * FROM sync_requests_tb
        WHERE date = '$date_for_sync_requests_tb'") );
        
        //Если мы получили новый уровень равный нулю для текущего дня
        //то проверяем старый уровень для этого же дня если он уже был задан то
        //не производим никаких действий
        //иначе записываем новый уровень.
        //Это нужно для предотвращения обнуления уровня при вторичной синхронизации
        //в текущий день если не было добавлено новых строк
        if($new_level == 0 AND $row_sync_req_tb['level'] > 0){
            //пусто
        }else{        
            
            //TEST
            //echo '$new_level: ' . $new_level . "\r\n";
             
            $this->q_sub("INSERT INTO sync_requests_tb (`date`, `level`) 
            VALUES ('$date_for_sync_requests_tb', $new_level)
            ON DUPLICATE KEY UPDATE level=$new_level");     
        }
    }
    
    //запись новой контрольной метки
    protected function update_control_timestamp($id){
        /*
        $this->q_sub("INSERT INTO sync_control_mark (`control_timestamp`) 
        VALUES ( FROM_UNIXTIME($this->new_control_timestamp) )
        ON DUPLICATE KEY UPDATE control_timestamp=VALUES(control_timestamp)   
        WHERE id = $id");
        */
        
        $this->q_sub("UPDATE sync_control_mark 
        SET control_timestamp = FROM_UNIXTIME($this->new_control_timestamp)
        WHERE id = $id");
    }
    
}
?>