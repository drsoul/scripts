<?php
###класс содержит функционал соединения с бд, а так же базовые методы
class Db_connect{      

    #главная бд
    protected $DB_HOST;
    protected $DB_USER;
    protected $DB_PASS;
    protected $DB_NAME;

    #дочерняя бд 
    protected $DB_HOST_SUB;
    protected $DB_USER_SUB;
    protected $DB_PASS_SUB;
    protected $DB_NAME_SUB;    

    protected $_server_dsp;    
    protected $_server_dsp_number;
    public $_sync_db_insert_portion_limit;
    public $_sync_db_delete_portion_limit;
    protected $_server_img;
    protected $_show_mysql_err;
    
    protected $_targets_transfer_table;        
    protected $_targets_transfer_path_dump;        
    protected $_targets_transfer_dump_pass;
    
    protected $_mysql_time_zone;
    protected $_mysql_time_zone_reverse;
    
    protected $count_aucs;
    
    protected $_sql_cache;
    
    protected $_memcached_net_host;
    protected $_memcached_net_port;
    
    ###################################
    ######НЕ РЕДАКТИРОВАТЬ ТО ЧТО НИЖЕ!
    ###################################
    
    ###ссылки на соединения с базами данных
    protected $main_bd;
    protected $sub_bd;
    
    private static $instance;
    
    //создаёт соединения с базами данных
    //и задаёт кодировки
    public function __construct() {
        
        #подключение конфига
        require_once( dirname(__FILE__) . '/config.php' );
        
        #ЗАПИСЬ ДАННЫХ ИЗ КОНФИГА 
        #главная бд        
        $this->DB_HOST = DB_HOST;
        $this->DB_USER = DB_USER;
        $this->DB_PASS = DB_PASS;
        $this->DB_NAME = DB_NAME;
        
        #дочерняя бд         
        $this->DB_HOST_SUB = DB_HOST_SUB;
        $this->DB_USER_SUB = DB_USER_SUB;
        $this->DB_PASS_SUB = DB_PASS_SUB;
        $this->DB_NAME_SUB = DB_NAME_SUB;
        
         
        
        /*
        ###рокеровка баз
        #главная бд
        $this->DB_HOST = DB_HOST_SUB;
        $this->DB_USER = DB_USER_SUB;
        $this->DB_PASS = DB_PASS_SUB;
        $this->DB_NAME = DB_NAME_SUB;
        
        #дочерняя бд         
        $this->DB_HOST_SUB = DB_HOST;
        $this->DB_USER_SUB = DB_USER;
        $this->DB_PASS_SUB = DB_PASS;
        $this->DB_NAME_SUB = DB_NAME;
        */
        
        #сервер обработки запросов
        #там где находятся скрипты аукциона, уведомления о победе, клика
        #аукцион направляет ssp на этот сервер, ssp обращается к скрипту уведомления о победе,
        #а пользователь сайта донора переходит на него после клика по баннеру
        $this->_server_dsp = SERVER_DSP;
        
        #номер dsp сервера
        $this->_server_dsp_number = SERVER_DSP_NUMBER;
        
        #размер порции (строк) INSERT во время синхронизации баз данных
        $this->_sync_db_insert_portion_limit = SYNC_DB_INSERT_PORTION_LIMIT;
        
        #описание в config.php
        $this->_sync_db_delete_portion_limit = SYNC_DB_DELETE_PORTION_LIMIT;
        
        #сервер с изображениями баннеров
        $this->_server_img = SERVER_IMG;

        #отображение mysql ошибок: 1 - да, 0 - нет.
        $this->_show_mysql_err = SHOW_MYSQL_ERR;
        
        #временная зона сервера mysql (необходима для верной работы критерия аукциона - часовых паясов)
        $this->_mysql_time_zone = MYSQL_TIME_ZONE; 
        #временная зона сервера mysql c обращённым знаком
        $this->_mysql_time_zone_reverse = $this->_mysql_time_zone * -1;  
        
        $this->_targets_transfer_table = TARGETS_TRANSFER_TABLE;
        
        $this->_targets_transfer_path_dump = TARGETS_TRANSFER_PATH_DUMP;
        
        $this->_targets_transfer_dump_pass = TARGETS_TRANSFER_DUMP_PASS;
        
        $this->_count_aucs = COUNT_AUCS;
        
        $this->_sql_cache = SQL_CACHE;
        
        $this->_memcached_net_host = MEMCACHED_NET_HOST;
        $this->_memcached_net_port = MEMCACHED_NET_PORT;
        #КОНЕЦ - ЗАПИСЬ ДАННЫХ ИЗ КОНФИГА
 
        
        #индификаторы соединений
        $this->main_bd = mysql_pconnect($this->DB_HOST, $this->DB_USER, $this->DB_PASS);
        //вернул обратно - #####ВРЕМЕННО МЕНЯЮ МЕСТАМИ main_bd и sub_bd для теста!!!!        
        $this->sub_bd = mysql_pconnect($this->DB_HOST_SUB, $this->DB_USER_SUB, $this->DB_PASS_SUB);
        
        mysql_select_db($this->DB_NAME, $this->main_bd);
        mysql_select_db($this->DB_NAME_SUB, $this->sub_bd);
        
        
        #установка кодировки
        
        /*
        mysql_query("set character_set_client='utf8'", $this->main_bd); 
        mysql_query("set character_set_results='utf8'", $this->main_bd); 
        mysql_query("set collation_connection='utf8_general_ci'", $this->main_bd);        
        
        mysql_query("set character_set_client='utf8'", $this->sub_bd); 
        mysql_query("set character_set_results='utf8'", $this->sub_bd); 
        mysql_query("set collation_connection='utf8_general_ci'", $this->sub_bd);
        */
        
        mysql_query("SET NAMES utf8", $this->main_bd);
        mysql_query("SET NAMES utf8", $this->sub_bd);
    }
    
    public static function I(){
        
        if (!isset(self::$instance)){
            self::$instance = new Db_connect();
        }
        return self::$instance;
    }
    
    //запрос к главной бд
    // params
    // $sql - sql код
    // return - такой же как и у функции mysql_query
    public function q_main($sql){
        
        //если включено отображение ошибок то в случае их наличия вывод их
        if($this->_show_mysql_err == 1){
            if( !( $res = mysql_query($sql, $this->main_bd) ) ){
                
                if( mysql_errno($this->main_bd) != 0 ){
                    echo '\r\n <br> <b style="color:red;">Fatal Mysql error.</b> ';
                    echo "\r\n <br> <b>Query for MAIN db.</b> ";
                    echo "\r\n <br> <b>DB name:</b> $this->DB_NAME";
                    echo "\r\n <br> <b>Error number:</b> " . mysql_errno($this->main_bd);
                    echo "\r\n <br> <b>Description:</b> " . mysql_error($this->main_bd);
                    echo "\r\n <br> <b>SQL:</b> " . $sql . "\r\n\r\n <br>";
                }
                return $res;
            }else{
                return $res;
            }
        //если отображение ошибок не включено то сразу возвращаем результат запроса
        }else{
            return mysql_query($sql, $this->main_bd);
        }
    }   
    
    //запрос к дочерней бд
    // params
    // $sql - sql код
    // return - такой же как и у функции mysql_query
    public function q_sub($sql){
        
        //если включено отображение ошибок то в случае их наличия вывод их
        if($this->_show_mysql_err == 1){
            if( !( $res = mysql_query($sql, $this->sub_bd) ) ){
                
                if( mysql_errno($this->sub_bd) != 0 ){
                    echo "\r\n <br> <b style='color:red;'>Fatal Mysql error.</b> ";
                    echo "\r\n <br> <b>Query for SUB db.</b> ";
                    echo "\r\n <br> <b>DB name:</b> $this->DB_NAME_SUB";                    
                    echo "\r\n <br> <b>Error number:</b> " . mysql_errno($this->sub_bd);
                    echo "\r\n <br> <b>Description:</b> " . mysql_error($this->sub_bd);
                    echo "\r\n <br> <b>SQL:</b> " . $sql . "\r\n\r\n <br>";
                }
                return $res;
            }else{
                return $res;
            }
        //если отображение ошибок не включено то сразу возвращаем результат запроса
        }else{
            return mysql_query($sql, $this->sub_bd);
        }
    }
    
    //выборка ассоциативного массива из результата запроса
    // params
    // $res - результат запроса mysql_query()
    // return - ассоциативный массив
    public function fetch_assoc($res){
        return mysql_fetch_assoc($res);
    }
    
    //выборка ассоциативного массива в главной базе
    // params
    // $sql - sql код
    // return - ассоциативный массив
    public function fetch_assoc_main($sql){
        return mysql_fetch_assoc($this->q_main($sql));
    }
    
    //выборка ассоциативного массива в дочерней базе
    // params
    // $sql - sql код
    // return - ассоциативный массив
    public function fetch_assoc_sub($sql){
        return mysql_fetch_assoc($this->q_sub($sql));
    }
    
    //возвращает последний вставленный id колонки AUTOINCREMENT для главной бд
    public function insert_id_main(){
        return mysql_insert_id($this->main_bd);
    }
    
    //возвращает последний вставленный id колонки AUTOINCREMENT для дочерней бд
    public function insert_id_sub(){
        return mysql_insert_id($this->sub_bd);
    }
    
    //выборка массив в главной базе
    // params
    // $res - результать выполнения функции mysql_query()
    // return - возвращает массив строк соответствующих выбранной строке набора
    // или NULL, если в результирующей таблице больше нет данных.
    public function fetch_array($res){
        return mysql_fetch_array($res);
    }
    
    //возвращает количество строк в результате запроса
    // params
    // mysql_result $res - результат выполнения запроса
    // return int - возвращает число рядов в результирующей выборке. 
    // если число рядов больше чем MAXINT, то число будет возвращено в виде строки.
    public function num_rows($res){
        return mysql_num_rows($res);
    }
    
    //Получение строки результирующей таблицы в виде массива
    // params
    // mysql_result $res - результат выполнения запроса
    // return array or null - возвращает массив строк, соответствующих данным
    // в выбранной строке результирующей таблицы, или NULL, если доступных строк больше нет.
    public function fetch_row($res){
        return mysql_fetch_row($res);
    }        
    
    //экранирует строку для главной бд
    public function escape_main($date){
        return mysql_real_escape_string($date, $this->main_bd);
    }
    
    //экранирует строку для дочерней бд
    public function escape_sub($date){
        return mysql_real_escape_string($date, $this->sub_bd);
    }
    
    public function affected_rows_sub(){
        return mysql_affected_rows($this->sub_bd);
    }
    
    public function affected_rows_main(){
        return mysql_affected_rows($this->main_bd);
    }
    
}